import python.parser.specparser as specparser
# TODO This calls exit(), so it needs to be restructured as a bunch of functions or a class
# import python.mutator.mutate as mutator
import python.compiler.junitcompiler as junitcompiler
import os, glob, shutil, python.util as util
from subprocess import *

class BVATester:
    '''
    The main class for executing our test suite.
    '''
    
    # Java Directories
    JAVADIR = 'java/'
    JAVASRCDIR = JAVADIR + 'src/'
    ORACLEDIR = JAVASRCDIR + 'oracles/'
    TESTDIR = JAVADIR + 'test/auto/'
    HUMANDIR = JAVADIR + 'test/human/'
    CLASSPATH = "java/lib/junit-4.8.1.jar:java/src/:java/test:java/test/auto:../"
    HUMANCLASSPATH = "java/lib/junit-4.8.1.jar:java/src/:java/test:java/test/human:../"

    # Python Directories
    PYTHONDIR = 'python/'
    PARSERDIR = PYTHONDIR + 'parser/'
    MUTATORDIR = PYTHONDIR + 'mutator/'
    MUTATIONDIR = MUTATORDIR + 'mutations/'
    COMPILERDIR = PYTHONDIR + 'compiler/'

    # A map of oracle class name to methods
    # The "Oracle" suffix is omitted here
    oracles = {
        'String' : ['indexOf'],
        'Binary' : ['binarySearch'],
        'LCM' : ['lcm'],
        'GCD' : ['gcd']
    }
    parameters = {}
    # Key should be an oracle, value should be an int count >= 0 meaning there are that many
    # The mutation list will eventually hold a boolean for each mutant of each oracle that indicates if that mutant was killed. True indicates killed.
    mutations = {}
    # humanMutations is exactly the same as mutations except it contains the results of mutation testing using the human BVA tests.
    humanMutations = {}
    # currently not using results list as most results are calculated directly during the traversal of the mutant list
    results = {}

    def __init__(self):
        for oracle in self.oracles:
            self.mutations[oracle] = {}
            self.humanMutations[oracle] = {}
            self.parameters[oracle] = {}
            #for method in self.oracles[oracle]:
            #   self.mutations[oracle][method] = 0
            #   self.parameters[oracle][method] = None
        # Compile all oracles
        util.shell('javac ' + self.ORACLEDIR + '/*.java')

    def run(self):
        '''
        The main method for running all submodules and returning results.
        '''
        self.__parse()
        #self.__mutate()
        self.__importMutants()
        self.__compile()
        self.__results()
        self.__humanResults()
        return self.results

    def __parse(self):
        '''
        Iterate over the oracles and generate Boundary Value Analysis parameters.
        '''
        for oracle in self.oracles:
            parser = specparser.SpecParser()
            self.parameters[oracle] = parser.generate_tests_file((self.ORACLEDIR + util.interfaceName(oracle, True)))
        pass

    def __mutate(self):
        '''
        Iterate over the oracles and mutate each a certain number of times, storing the mutations 
        in MUTATIONDIR and returning the number of mutations. 
        Currently empty since we should not have to generate mutants on every single invocation of this script. 
        Instead they are assumed to have been pregenerated using the mutator script 
        '''
        pass

    def __importMutants(self):
        '''
        Imports the mutants from MUTATIONDIR into the mutations list. 
        '''
        for oracle in self.oracles:
            files = glob.glob((self.MUTATIONDIR+util.oracleName(oracle))+'/*.java')
            dirname=(self.MUTATIONDIR+util.oracleName(oracle))
            for filename in files:
                    self.mutations[oracle][(filename)] = None
                    self.humanMutations[oracle][(filename)] = None
                
        pass

    def __compile(self):
        '''
        Iterate over existing mutations and generate valid JUnit test drivers using the parameters.
        '''
        for oracle in self.oracles:
            # NOTE: Only executes the first method in each oracle for now
            compiler = junitcompiler.JUnitCompiler()
            compiler.compilerDir = self.COMPILERDIR
            compiler.javaDir = self.JAVASRCDIR
            compiler.oracleDir = self.ORACLEDIR
            driver = compiler.compile(oracle, self.oracles[oracle][0], self.parameters[oracle])
            f = open((self.TESTDIR + util.testName(oracle, True)), 'w+')
            f.write(driver)
            f.close()
        #compile the new test drivers
        util.shell('javac ' + '-classpath ' + self.CLASSPATH + ' ' + self.TESTDIR + '/*.java')

    def __results(self):

        util.shell('javac ' + self.ORACLEDIR + '/*.java')
        print 'Results for automatic Boundary Value Analysis\n---'
        #java -classpath ../../lib/junit-4.8.1.jar:../../src/:../java/test:../auto:../ org.junit.runner.JUnitCore auto.GCDTest
        for oracle in self.oracles:
            print (oracle+' Mutation Tests\t\t' + 'jUnit output')
            output = util.shell('java -classpath ' + self.CLASSPATH + ' org.junit.runner.JUnitCore auto.' + util.testName(oracle, False))
            #Get the junit results for the oracle to compare with the mutant results
            oracleTestResults = output.split('\n')[1]
            print (util.oracleName(oracle, True)+'\t\t\t' + oracleTestResults + '\n')

            pathToOracle = (self.ORACLEDIR + util.oracleName(oracle, True))
            #restores oracle in case last execution of this script didn't complete
            if os.path.isfile((pathToOracle+'.tmp')):
                shutil.copyfile((pathToOracle+'.tmp'), pathToOracle)
                os.remove((pathToOracle+'.tmp'))

            #save oracle into a tmp file to be restored later
            shutil.copyfile(pathToOracle, (pathToOracle+'.tmp'))
            
            mutantCount=0
            killCount=0
            for mutation in self.mutations[oracle]:
                mutantCount+=1
                #copy mutation to correct location and recompile.
                shutil.copyfile(mutation, pathToOracle)
                util.shell('javac ' + self.ORACLEDIR + '/*.java')

                #get junit output for compiled mutant
                mutantoutput = util.shell('java -classpath ' + self.CLASSPATH + ' org.junit.runner.JUnitCore auto.' + util.testName(oracle, False))
                if (mutantoutput.split('\n')[1] != oracleTestResults):
                    #mutant has been killed if True
                    self.mutations[oracle][mutation] = True
                    killCount+=1
                else:
                    self.mutations[oracle][mutation] = False
                
                head, tail = os.path.split(mutation)
                killStatus = 'Killed.' if (self.mutations[oracle][mutation]) else 'Still Alive.'
                print (tail + ' \t\t'+ (mutantoutput.split('\n')[1]) +'\t\t'+ killStatus)

            #restore oracle
            os.remove(pathToOracle)
            shutil.copyfile((pathToOracle+'.tmp'), pathToOracle)
            os.remove((pathToOracle+'.tmp'))

            #report results
            print (oracle+': ' + str(killCount) +'/'+ str(mutantCount) +' mutants killed.\n')

    def __humanResults(self):

        for dirname, dirnames, filenames in os.walk((self.HUMANDIR)):
            #for each user directory in human tests
            for direc in dirnames:
                for dirname2, dirnames2, filenames2 in os.walk((self.HUMANDIR+direc)):
                    for fname in filenames2:
                        #copy each test into the parent human directory
                        shutil.copy((self.HUMANDIR+direc+'/'+fname),(self.HUMANDIR))

                #compile human drivers
                util.shell('javac ' + '-classpath ' + self.HUMANCLASSPATH + ' ' + self.HUMANDIR + '/*.java')
                util.shell('javac ' + self.ORACLEDIR + '/*.java')

                print ('Results for human Boundary Value Analysis - user: '+direc+'\n---')
                #java -classpath ../../lib/junit-4.8.1.jar:../../src/:../java/test:../auto:../ org.junit.runner.JUnitCore auto.GCDTest
                for oracle in self.oracles:
                    print (oracle+' Mutation Tests\t\t' + 'jUnit output')
                    output = util.shell('java -classpath ' + self.HUMANCLASSPATH + ' org.junit.runner.JUnitCore human.' + util.testName(oracle, False))
                    #Get the junit results for the oracle to compare with the mutant results
                    oracleTestResults = output.split('\n')[1]
                    print (util.oracleName(oracle, True)+'\t\t\t' + oracleTestResults + '\n')

                    pathToOracle = (self.ORACLEDIR + util.oracleName(oracle, True))
                    #restores oracle in case last execution of this script didn't complete
                    if os.path.isfile((pathToOracle+'.tmp')):
                        shutil.copyfile((pathToOracle+'.tmp'), pathToOracle)
                        os.remove((pathToOracle+'.tmp'))

                    #save oracle into a tmp file to be restored later
                    shutil.copyfile(pathToOracle, (pathToOracle+'.tmp'))
                    
                    mutantCount=0
                    killCount=0
                    for mutation in self.humanMutations[oracle]:
                        mutantCount+=1
                        #copy mutation to correct location and recompile.
                        shutil.copyfile(mutation, pathToOracle)
                        util.shell('javac ' + self.ORACLEDIR + '/*.java')

                        #get junit output for compiled mutant
                        mutantoutput = util.shell('java -classpath ' + self.HUMANCLASSPATH + ' org.junit.runner.JUnitCore human.' + util.testName(oracle, False))
                        if (mutantoutput.split('\n')[1] != oracleTestResults):
                            #mutant has been killed if True
                            self.humanMutations[oracle][mutation] = True
                            killCount+=1
                        else:
                            self.humanMutations[oracle][mutation] = False
                        
                        head, tail = os.path.split(mutation)
                        killStatus = 'Killed.' if (self.humanMutations[oracle][mutation]) else 'Still Alive.'
                        print (tail + ' \t\t'+ (mutantoutput.split('\n')[1]) +'\t\t'+ killStatus)

                    #restore oracle
                    os.remove(pathToOracle)
                    shutil.copyfile((pathToOracle+'.tmp'), pathToOracle)
                    os.remove((pathToOracle+'.tmp'))

                    #report results
                    print (oracle+': ' + str(killCount) +'/'+ str(mutantCount) +' mutants killed.\n')
                
                #clean up by deleting the test drivers that were copied from their user directory earlier
                files = glob.glob(self.HUMANDIR+'*.java') + glob.glob(self.HUMANDIR+'*.class')
                for f in files:
                    #print f
                    os.remove(f)

# A sample execution, could be moved outside at some point
tester = BVATester()
tester.run()
#print "parameters : ", tester.parameters
#print "mutations : ", tester.mutations
#print "results : ", tester.results
