package oracles;

import java.util.ArrayList;
import java.util.List;

/**
 * An execution class for StringOracle.
 * 
 * @see Execution
 * @author aramk
 */
public class StringExec extends Execution {

    public static void main(String args[]) {
        ArrayList<Integer> result = execArgs(2, args, null,
                new Callable<List<String>, Integer>() {
                    public Integer call(List<String> input) {
                        return indexOf(input.get(0), input.get(1));
                    }
                });
        System.out.println(join(result));
    }

    public static int indexOf(String str, String sub) {
        StringOracle strOracle = StringOracle.fromString(str);
        StringOracle subOracle = StringOracle.fromString(sub);
        return strOracle.indexOf(subOracle);
    }

}
