package oracles;

/**
 * Command pattern. <I> is the input type, <O> the output type.
 * 
 * @author aramk
 */
public interface Callable<I, O> {

    public O call(I input);

}
