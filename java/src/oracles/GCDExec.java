package oracles;

import java.util.ArrayList;
import java.util.List;

/**
 * An execution class for GCD
 * 
 * @see Execution
 * @author jismith
 */
public class GCDExec extends Execution {

    public static void main(String args[]) {

        ArrayList<Integer> result = execArgs(2, args, null,
                new Callable<List<String>, Integer>() {
                    public Integer call(List<String> input) {
                        return gcd(Integer.parseInt(input.get(0)),
                                Integer.parseInt(input.get(1)));
                    }
                });
        System.out.println(join(result));
    }

    public static int gcd(int a, int b) {
        GCDOracle gcdo = new GCDOracle();
        int result = gcdo.gcd(a, b);

        return result;
    }

}
