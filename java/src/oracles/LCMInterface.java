package oracles;

public interface LCMInterface {

	/**
     * Lowest Common multiple of two integers.
     * 
     * @param m [0 <= \1]
     *     The first number
     * @param n [0 <= \2]
     *     The second number
     * @return
     *     The LCM, which is zero if either m or n is.
     */
    public int lcm(int m, int n);
}
