package oracles;

public interface BinaryInterface {
	
	/**
	 * Search for the given check in the array nums.
	 *
	 * @param nums
	 *     The list to search in
     * @param check
     *     The number to search for
	 * @return [contains(\2, \1)]
     *     Index of the given target.
	 */
    public int binarySearch(int[] nums, int check);

}
