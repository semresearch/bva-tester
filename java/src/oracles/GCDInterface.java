package oracles;

public interface GCDInterface {

	/**
     * Greatest Common divisor of two positive integers.
     *
     * @param numa [0 < \1]
     *     First number
     * @param numb [0 < \2]
     *     Second number 
     * @return
     *     The GCD of the two, or the other one if one is zero.
     */
	public int gcd(int numa, int numb);
}
