package oracles;

import java.util.ArrayList;
import java.util.List;

/**
 * An execution class for LCM
 * 
 * @see Execution
 * @author aramk
 */
public class LCMExec extends Execution {

    public static void main(String args[]) {
        
        ArrayList<Integer> result = execArgs(2, args,
                new Callable<String, Integer>() {
                    public Integer call(String input) {
                        return Integer.parseInt(input);
                    }
                }, new Callable<List<Integer>, Integer>() {
                    public Integer call(List<Integer> input) {
                        return lcm(input.get(0), input.get(1));
                    }
                });

        System.out.println(join(result));

    }


    /**
     * Lowest Common divisor of two integers.
     * 
     * @param numa
     *            \d+ [0 <= \1] The hour of the day
     * @param numb
     *            \d+ [0 <= \2] The minute of the hour
     * @return The GCD
     */
    public static int lcm(int m, int n) {
        LCMOracle lcm = new LCMOracle();
        return lcm.lcm(m, n);
    }

}
