package oracles;

import java.util.ArrayList;

/**
 * An execution class for BinaryOracle.
 * 
 * Takes a list of args, uses the first as the search key and the rest as the
 * target array. Note that arg index 0 is the name of the method to execute, but
 * since we only have a single method at the moment we don't need to use it.
 * 
 * @see Execution
 * @author jismith, aramk
 */
public class BinaryExec extends Execution {

    public static int binarySearch(int[] nums, int check) {
        BinaryOracle bo = new BinaryOracle();
        return bo.binarySearch(nums, check);
    }

    public static void main(String args[]) {

        ArrayList<Integer> results = new ArrayList<Integer>();

        /**
         * Treat every first argument as a formatted int[]. Parse it and use
         * every second argument as the query.
         */
        for (int i = 1; i < args.length; i++) {
            if (i % 2 == 1) {
                int query = Integer.parseInt(args[i + 1]);
                String arrayStr = args[i];
                arrayStr = arrayStr.trim();
                String[] strArgs = arrayStr.substring(1, arrayStr.length() - 1)
                        .trim().split("\\s*,\\s*");
                int[] target = new int[strArgs.length];
                for (int j = 0; j < strArgs.length; j++) {
                    target[j] = Integer.parseInt(strArgs[j]);
                }
                int result = binarySearch(target, query);
                results.add(result);
            }
        }

        System.out.println(join(results));
    }

}
