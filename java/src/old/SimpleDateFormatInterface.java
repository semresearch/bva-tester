package old;

public interface SimpleDateFormatInterface {

    
    /**
	 * Converts integer time values to stringy date representation.
	 *
	 * @param hours \d+ [0 <= \1 < 24]
	 *     The hour of the day
	 * @param minutes \d+ [0 <= \2 < 60]
	 *     The minute of the hour 
	 * @param seconds \d+ [0 <= \3 < 60]
	 *     The second of the minute
	 * @return String like "HH:MM:SS"
	 */

    public String format(int hour, int min, int sec);

}
