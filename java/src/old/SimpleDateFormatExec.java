package old;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import oracles.Callable;
import oracles.Execution;

/**
 * An execution class for SimpleDateFormatOracle.
 * 
 * @see Execution
 * @author aramk
 */
public class SimpleDateFormatExec extends Execution {

    private static final SimpleDateFormatOracle df = new SimpleDateFormatOracle(
            "HH:mm:ss");

    public static void main(String args[]) {
        if (args[0].equals("parse")) {
            ArrayList<Date> result = execArgs(1, args, null,
                    new Callable<List<String>, Date>() {
                        public Date call(List<String> input) {
                            return parse(input.get(0));
                        }
                    });
            System.out.println(join(result));
        } else if (args[0].equals("format")) {
            ArrayList<String> result = execArgs(3, args,
                    new Callable<String, Integer>() {
                        public Integer call(String input) {
                            return Integer.parseInt(input);
                        }
                    }, new Callable<List<Integer>, String>() {
                        public String call(List<Integer> input) {
                            return format(input.get(0).intValue(), input.get(1)
                                    .intValue(), input.get(2).intValue());
                        }
                    });
            System.out.println(join(result));
        }
    }

    public static Date parse(String date) {
        Date result = null;
        try {
            result = df.parse(date);
        } catch (ParseException e) {

        }
        return result;
    }

    public static String format(int hour, int min, int sec) {
        return df.format(hour, min, sec);
    }

}
