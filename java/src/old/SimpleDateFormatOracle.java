package old;

import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class SimpleDateFormatOracle extends SimpleDateFormat implements
        SimpleDateFormatInterface {

    // TODO this isn't a mutable oracle at the moment

    public SimpleDateFormatOracle(String string) {
        super(string);
    }

    @SuppressWarnings("deprecation")
    public String format(int hour, int min, int sec) {
        Date date = new Date();
        date.setHours(hour);
        date.setMinutes(min);
        date.setSeconds(sec);
        return format(date);
    }

    // I've tried redefining the java.text package files(not sure if that is
    // actually possible)
    // the main issue I can see is a permission problem with the sun.util
    // package/locale stuff

    // format and subformat methods would be pasted in here.

}
