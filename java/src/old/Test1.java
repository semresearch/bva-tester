//package oracles;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * A helper class for performing execution of oracles.
// * 
// * Subclasses should implement a static main() method to provide an interface
// * for executing the oracle from the command line. This allows expected outputs
// * to be obtained by the oracle compiler for generating test cases. All other
// * methods must be static and are pasted into the generated JUnit driver as
// * utility methods for invoking the oracle during assertion checks in the test
// * cases.
// * 
// * @author aramk
// */
//public class Test1 {
//
//    /**
//     * Joins a list of objects with glue in between into a String.
//     */
//    public static  String join(List objs, String glue) {
//        String arrayStr = "";
//        for (int i = 0; i < objs.size(); i++) {
//            arrayStr += objs.get(i);
//            if (i < objs.size() - 1) {
//                arrayStr += glue;
//            }
//        }
//        return arrayStr;
//    }
//
//    public static  String join(List objs) {
//        return join(objs, " ");
//    }
//
//    /**
//     * Calls the oracle method with the given arguments. Allows you to pass
//     * several sets of arguments in a single array and call the oracle method on
//     * each set, passing back the array of outputs.
//     * 
//     *  is the input type.  is the output type of the oracle method.
//     * 
//     * @param expectedArgCount
//     *            The number of arguments the oracle method expects
//     * @param args
//     *            The actual command line arguments passed into main() of type
//     *            String. This can either equal the expectedArgCount, or it can
//     *            exceed it, in which case each set of expected arguments are
//     *            used. This array is parsed and sent as arguments into
//     *            callMethod.
//     * @param convertArg
//     *            An inline class for converting an argument in args of type
//     *            String into an argument of type . If  is type String,
//     *            this parameter can be null.
//     * @param callMethod
//     *            An inline class for calling the oracle method
//     * @return An array of outputs from the oracle method. If passing exactly
//     *         the required number of arguments, the output only contains a
//     *         single element.
//     */
//    public static  ArrayList execArgs(int expectedArgCount,
//            String[] args, Callable convertArg,
//            Callable callMethod) {
//        ArrayList argBuffer = new ArrayList();
//        ArrayList results = new ArrayList();
//        boolean correctArgCount = expectedArgCount == args.length - 1;
//        // Group arguments and get results
//        for (int i = 1; i < args.length; i++) {
//            A arg;
//            if (convertArg != null) {
//                arg = convertArg.call(args[i]);
//            } else {
//                arg = (A) args[i];
//            }
//            argBuffer.add(arg);
//            if (!correctArgCount && argBuffer.size() == expectedArgCount) {
//                results.add(callMethod.call(argBuffer));
//                argBuffer.clear();
//            }
//        }
//        if (correctArgCount) {
//            results.add(callMethod.call(argBuffer));
//        }
//        return results;
//    }
//
//}
