package old;
import java.util.Date;

/**
 * Sample code used for mutation testing
 *  
 * @deprecated
 */
public interface SEMTests {
    /**
     * Simple binary search on an array of primitive ints
     * @param nums An array of ints
     * @param check The item in the array to find 
     * @return
     */
    public int binarySearch(int[] nums, int check);
    
    /**
     * Formats a date string in HH:mm:ss format
     * @param hour The hour (0-23)
     * @param min The minute (0-59)
     * @param sec The second (0-59)
     * @return The formatted date string in HH:mm:ss format
     */
    public String dateFormat(int hour, int min, int sec);
    
    /**
     * Converts a date string to a Date object 
     * @param date A date String in HH:mm:ss format
     * @return The converted Date object
     */
    public Date dateParse(String date);
    
    /**
     * Searches a string for the index of a substring
     * @param str The target string
     * @param sub The substring/query string
     * @return The index of the substring, else -1
     */
    public int indexOf(String str, String sub);
}
 