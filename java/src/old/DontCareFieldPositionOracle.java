package old;

/*
 * %W% %E%
 *
 * Copyright (c) 2006, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
 * DontCareFieldPosition defines no-op FieldDelegate. Its
 * singleton is used for the format methods that don't take a
 * FieldPosition.
 */
class DontCareFieldPositionOracle extends FieldPositionOracle {
    // The singleton of DontCareFieldPosition.
    static final FieldPositionOracle INSTANCE = new DontCareFieldPositionOracle();

    private final FormatOracle.FieldDelegate noDelegate = new FormatOracle.FieldDelegate() {
        public void formatted(FormatOracle.Field attr, Object value, int start,
                              int end, StringBuffer buffer) {
        }
        public void formatted(int fieldID, FormatOracle.Field attr, Object value,
                              int start, int end, StringBuffer buffer) {
        }
    };

    private DontCareFieldPositionOracle() {
        super(0);
    }

    FormatOracle.FieldDelegate getFieldDelegate() {
        return noDelegate;
    }
}
