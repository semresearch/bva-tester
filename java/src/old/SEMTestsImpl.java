package old;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import oracles.StringOracle;

/**
 * @deprecated
 */
public class SEMTestsImpl implements SEMTests {

	public int binarySearch(int[] nums, int check) {
		return binarySearch2(nums, check, 0, nums.length);
	}

	private int binarySearch2(int[] nums, int check, int lo, int hi) {
		if (hi < lo) {
			return -1; // impossible index for "not found"
		}
		int guess = (hi + lo) / 2;
		if (nums[guess] > check) {
			return binarySearch2(nums, check, lo, guess - 1);
		} else if (nums[guess] < check) {
			return binarySearch2(nums, check, guess + 1, hi);
		}
		return guess;
	}

	public String dateFormat(int hour, int min, int sec) {
		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		date.setHours(hour);
		date.setMinutes(min);
		date.setSeconds(sec);
		return df.format(date);
	}

	public Date dateParse(String date) {
		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
		Date result = null;
		try {
			result = df.parse(date);
		} catch (ParseException e) {

		}
		return result;
	}

	public int indexOf(String str, String sub) {
		StringOracle strOracle = StringOracle.fromString(str);
		StringOracle subOracle = StringOracle.fromString(sub);
		return strOracle.indexOf(subOracle);
	}

	public static void main(String[] args) {
		SEMTestsImpl tests = new SEMTestsImpl();

		int[] list = new int[] { 1, 2, 3 };
		int result = tests.binarySearch(list, 2);
		System.out.println(result);

		String date = tests.dateFormat(10, 30, 40);
		System.out.println(date);

		Date date2 = tests.dateParse("10:30:40");
		System.out.println(date2);

		int sub = tests.indexOf("spongebob", "bob");
		System.out.println(sub);
	}

}
