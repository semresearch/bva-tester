//package old;
//
//import java.text.DateFormat;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.Locale;
//import java.util.TimeZone;
//
//import old.FormatOracle.FieldDelegate;
//import sun.util.calendar.CalendarUtils;
//import sun.util.calendar.ZoneInfoFile;
////import static java.text.SimpleDateFormat.PATTERN_INDEX_TO_CALENDAR_FIELD;
////import static java.text.SimpleDateFormat.PATTERN_INDEX_TO_DATE_FORMAT_FIELD;
////import static java.text.SimpleDateFormat.PATTERN_INDEX_TO_DATE_FORMAT_FIELD_ID;
////import static java.text.SimpleDateFormat.TAG_QUOTE_ASCII_CHAR;
////import static java.text.SimpleDateFormat.TAG_QUOTE_CHARS;
////import java.text.FieldPositionOracle;
//
//
//public class SimpleDateFormatOracle2 extends SimpleDateFormat {
//    
//    private final static int TAG_QUOTE_ASCII_CHAR       = 100;
//    private final static int TAG_QUOTE_CHARS            = 101;
//    
//    private static final int[] PATTERN_INDEX_TO_CALENDAR_FIELD =
//        {
//            Calendar.ERA, Calendar.YEAR, Calendar.MONTH, Calendar.DATE,
//            Calendar.HOUR_OF_DAY, Calendar.HOUR_OF_DAY, Calendar.MINUTE,
//            Calendar.SECOND, Calendar.MILLISECOND, Calendar.DAY_OF_WEEK,
//            Calendar.DAY_OF_YEAR, Calendar.DAY_OF_WEEK_IN_MONTH,
//            Calendar.WEEK_OF_YEAR, Calendar.WEEK_OF_MONTH,
//            Calendar.AM_PM, Calendar.HOUR, Calendar.HOUR, Calendar.ZONE_OFFSET,
//            Calendar.ZONE_OFFSET
//        };
//    
// // Map index into pattern character string to DateFormat field number
//    private static final int[] PATTERN_INDEX_TO_DATE_FORMAT_FIELD = {
//        DateFormat.ERA_FIELD, DateFormat.YEAR_FIELD, DateFormat.MONTH_FIELD,
//        DateFormat.DATE_FIELD, DateFormat.HOUR_OF_DAY1_FIELD,
//        DateFormat.HOUR_OF_DAY0_FIELD, DateFormat.MINUTE_FIELD,
//        DateFormat.SECOND_FIELD, DateFormat.MILLISECOND_FIELD,
//        DateFormat.DAY_OF_WEEK_FIELD, DateFormat.DAY_OF_YEAR_FIELD,
//        DateFormat.DAY_OF_WEEK_IN_MONTH_FIELD, DateFormat.WEEK_OF_YEAR_FIELD,
//        DateFormat.WEEK_OF_MONTH_FIELD, DateFormat.AM_PM_FIELD,
//        DateFormat.HOUR1_FIELD, DateFormat.HOUR0_FIELD,
//        DateFormat.TIMEZONE_FIELD, DateFormat.TIMEZONE_FIELD,
//    };
//
//    // Maps from DecimalFormatSymbols index to Field constant
//    private static final Field[] PATTERN_INDEX_TO_DATE_FORMAT_FIELD_ID = {
//        Field.ERA, Field.YEAR, Field.MONTH, Field.DAY_OF_MONTH,
//        Field.HOUR_OF_DAY1, Field.HOUR_OF_DAY0, Field.MINUTE,
//        Field.SECOND, Field.MILLISECOND, Field.DAY_OF_WEEK,
//        Field.DAY_OF_YEAR, Field.DAY_OF_WEEK_IN_MONTH,
//        Field.WEEK_OF_YEAR, Field.WEEK_OF_MONTH,
//        Field.AM_PM, Field.HOUR1, Field.HOUR0, Field.TIME_ZONE,
//        Field.TIME_ZONE,
//    };
//
//    private static final long serialVersionUID = -3298961797901398028L;
//
//    private DateFormatSymbolsOracle formatData;
//    
//    transient boolean isZoneStringsSet = false;
//    
//    private Locale locale;
//    
//    transient private char zeroDigit;
//    
//    transient private char[] compiledPattern;
//    
//    public StringBuffer format(Date date, StringBuffer toAppendTo,
//            FieldPositionOracle pos) {
//        pos.beginIndex = pos.endIndex = 0;
//        return format(date, toAppendTo, pos.getFieldDelegate());
//    }
//    
//    private final String getCalendarName() {
//        return calendar.getClass().getName();
//    }
//    
//    transient boolean useDateFormatSymbols;
//    
//    private boolean useDateFormatSymbols() {
//        if (useDateFormatSymbols) {
//            return true;
//        }
//        return isGregorianCalendar() || locale == null;
//    }
//
//    private boolean isGregorianCalendar() {
//        return "java.util.GregorianCalendar".equals(getCalendarName());
//    }
//
// // Called from Format after creating a FieldDelegate
//    private StringBuffer format(Date date, StringBuffer toAppendTo,
//                                FieldDelegate delegate) {
//        // Convert input date to time field list
//        calendar.setTime(date);
//
//        boolean useDateFormatSymbols = useDateFormatSymbols();
//
//        for (int i = 0; i < compiledPattern.length; ) {
//            int tag = compiledPattern[i] >>> 8;
//            int count = compiledPattern[i++] & 0xff;
//            if (count == 255) {
//                count = compiledPattern[i++] << 16;
//                count |= compiledPattern[i++];
//            }
//
//            switch (tag) {
//            case TAG_QUOTE_ASCII_CHAR:
//                toAppendTo.append((char)count);
//                break;
//
//            case TAG_QUOTE_CHARS:
//                toAppendTo.append(compiledPattern, i, count);
//                i += count;
//                break;
//
//            default:
//                subFormat(tag, count, delegate, toAppendTo, useDateFormatSymbols);
//                break;
//            }
//        }
//        return toAppendTo;
//    }
//    
//    /**
//     * Private member function that does the real date/time formatting.
//     */
//    private void subFormat(int patternCharIndex, int count,
//                           FieldDelegate delegate, StringBuffer buffer,
//                           boolean useDateFormatSymbols)
//    {
//        int     maxIntCount = Integer.MAX_VALUE;
//        String  current = null;
//        int     beginOffset = buffer.length();
//
//        int field = PATTERN_INDEX_TO_CALENDAR_FIELD[patternCharIndex];
//        int value = calendar.get(field);
//        int style = (count >= 4) ? Calendar.LONG : Calendar.SHORT;
//        if (!useDateFormatSymbols) {
//            current = calendar.getDisplayName(field, style, locale);
//        }
//
//        // Note: zeroPaddingNumber() assumes that maxDigits is either
//        // 2 or maxIntCount. If we make any changes to this,
//        // zeroPaddingNumber() must be fixed.
//
//        switch (patternCharIndex) {
//        case 0: // 'G' - ERA
//            if (useDateFormatSymbols) {
//                String[] eras = formatData.getEras();
//                if (value < eras.length)
//                    current = eras[value];
//            }
//            if (current == null)
//                current = "";
//            break;
//
//        case 1: // 'y' - YEAR
//            if (calendar instanceof GregorianCalendar) {
//                if (count >= 4)
//                    zeroPaddingNumber(value, count, maxIntCount, buffer);
//                else // count < 4
//                    zeroPaddingNumber(value, 2, 2, buffer); // clip 1996 to 96
//            } else {
//                if (current == null) {
//                    zeroPaddingNumber(value, style == Calendar.LONG ? 1 : count,
//                                      maxIntCount, buffer);
//                }
//            }
//            break;
//
//        case 2: // 'M' - MONTH
//            if (useDateFormatSymbols) {
//                String[] months;
//                if (count >= 4) {
//                    months = formatData.getMonths();
//                    current = months[value];
//                } else if (count == 3) {
//                    months = formatData.getShortMonths();
//                    current = months[value];
//                }
//            } else {
//                if (count < 3) {
//                    current = null;
//                }
//            }
//            if (current == null) {
//                zeroPaddingNumber(value+1, count, maxIntCount, buffer);
//            }
//            break;
//
//        case 4: // 'k' - HOUR_OF_DAY: 1-based.  eg, 23:59 + 1 hour =>> 24:59
//            if (current == null) {
//                if (value == 0)
//                    zeroPaddingNumber(calendar.getMaximum(Calendar.HOUR_OF_DAY)+1,
//                                      count, maxIntCount, buffer);
//                else
//                    zeroPaddingNumber(value, count, maxIntCount, buffer);
//            }
//            break;
//
//        case 9: // 'E' - DAY_OF_WEEK
//            if (useDateFormatSymbols) {
//                String[] weekdays;
//                if (count >= 4) {
//                    weekdays = formatData.getWeekdays();
//                    current = weekdays[value];
//                } else { // count < 4, use abbreviated form if exists
//                    weekdays = formatData.getShortWeekdays();
//                    current = weekdays[value];
//                }
//            }
//            break;
//
//        case 14:    // 'a' - AM_PM
//            if (useDateFormatSymbols) {
//                String[] ampm = formatData.getAmPmStrings();
//                current = ampm[value];
//            }
//            break;
//
//        case 15: // 'h' - HOUR:1-based.  eg, 11PM + 1 hour =>> 12 AM
//            if (current == null) {
//                if (value == 0)
//                    zeroPaddingNumber(calendar.getLeastMaximum(Calendar.HOUR)+1,
//                                      count, maxIntCount, buffer);
//                else
//                    zeroPaddingNumber(value, count, maxIntCount, buffer);
//            }
//            break;
//
//        case 17: // 'z' - ZONE_OFFSET
//            if (current == null) {
//                if (formatData.locale == null || formatData.isZoneStringsSet) {
//                    int zoneIndex =
//                        formatData.getZoneIndex(calendar.getTimeZone().getID());
//                    if (zoneIndex == -1) {
//                        value = calendar.get(Calendar.ZONE_OFFSET) +
//                            calendar.get(Calendar.DST_OFFSET);
//                        buffer.append(ZoneInfoFile.toCustomID(value));
//                    } else {
//                        int index = (calendar.get(Calendar.DST_OFFSET) == 0) ? 1: 3;
//                        if (count < 4) {
//                            // Use the short name
//                            index++;
//                        }
//                        String[][] zoneStrings = formatData.getZoneStringsWrapper();
//                        buffer.append(zoneStrings[zoneIndex][index]);
//                    }
//                } else {
//                    TimeZone tz = calendar.getTimeZone();
//                    boolean daylight = (calendar.get(Calendar.DST_OFFSET) != 0);
//                    int tzstyle = (count < 4 ? TimeZone.SHORT : TimeZone.LONG);
//                    buffer.append(tz.getDisplayName(daylight, tzstyle, formatData.locale));
//                }
//            }
//            break;
//
//        case 18: // 'Z' - ZONE_OFFSET ("-/+hhmm" form)
//            value = (calendar.get(Calendar.ZONE_OFFSET) +
//                     calendar.get(Calendar.DST_OFFSET)) / 60000;
//
//            int width = 4;
//            if (value >= 0) {
//                buffer.append('+');
//            } else {
//                width++;
//            }
//
//            int num = (value / 60) * 100 + (value % 60);
//            CalendarUtils.sprintf0d(buffer, num, width);
//            break;
//
//        default:
//            // case 3: // 'd' - DATE
//            // case 5: // 'H' - HOUR_OF_DAY:0-based.  eg, 23:59 + 1 hour =>> 00:59
//            // case 6: // 'm' - MINUTE
//            // case 7: // 's' - SECOND
//            // case 8: // 'S' - MILLISECOND
//            // case 10: // 'D' - DAY_OF_YEAR
//            // case 11: // 'F' - DAY_OF_WEEK_IN_MONTH
//            // case 12: // 'w' - WEEK_OF_YEAR
//            // case 13: // 'W' - WEEK_OF_MONTH
//            // case 16: // 'K' - HOUR: 0-based.  eg, 11PM + 1 hour =>> 0 AM
//            if (current == null) {
//                zeroPaddingNumber(value, count, maxIntCount, buffer);
//            }
//            break;
//        } // switch (patternCharIndex)
//
//        if (current != null) {
//            buffer.append(current);
//        }
//
//        int fieldID = PATTERN_INDEX_TO_DATE_FORMAT_FIELD[patternCharIndex];
//        Field f = PATTERN_INDEX_TO_DATE_FORMAT_FIELD_ID[patternCharIndex];
//
//        delegate.formatted(fieldID, f, f, beginOffset, buffer.length(), buffer);
//    }
//    
//    /**
//     * Formats a number with the specified minimum and maximum number of digits.
//     */
//    private final void zeroPaddingNumber(int value, int minDigits, int maxDigits, StringBuffer buffer)
//    {
//        // Optimization for 1, 2 and 4 digit numbers. This should
//        // cover most cases of formatting date/time related items.
//        // Note: This optimization code assumes that maxDigits is
//        // either 2 or Integer.MAX_VALUE (maxIntCount in format()).
//        try {
//            if (zeroDigit == 0) {
//                zeroDigit = ((DecimalFormat)numberFormat).getDecimalFormatSymbols().getZeroDigit();
//            }
//            if (value >= 0) {
//                if (value < 100 && minDigits >= 1 && minDigits <= 2) {
//                    if (value < 10) {
//                        if (minDigits == 2) {
//                            buffer.append(zeroDigit);
//                        }
//                        buffer.append((char)(zeroDigit + value));
//                    } else {
//                        buffer.append((char)(zeroDigit + value / 10));
//                        buffer.append((char)(zeroDigit + value % 10));
//                    }
//                    return;
//                } else if (value >= 1000 && value < 10000) {
//                    if (minDigits == 4) {
//                        buffer.append((char)(zeroDigit + value / 1000));
//                        value %= 1000;
//                        buffer.append((char)(zeroDigit + value / 100));
//                        value %= 100;
//                        buffer.append((char)(zeroDigit + value / 10));
//                        buffer.append((char)(zeroDigit + value % 10));
//                        return;
//                    }
//                    if (minDigits == 2 && maxDigits == 2) {
//                        zeroPaddingNumber(value % 100, 2, 2, buffer);
//                        return;
//                    }
//                }
//            }
//        } catch (Exception e) {
//        }
//
//        numberFormat.setMinimumIntegerDigits(minDigits);
//        numberFormat.setMaximumIntegerDigits(maxDigits);
//        numberFormat.format((long)value, buffer, DontCareFieldPositionOracle.INSTANCE);
//    }
//    
//}
