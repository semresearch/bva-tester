package human;

import static org.junit.Assert.assertEquals;
import oracles.GCDOracle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GCDTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    //The only oracle method.
    public int gcd(int numa, int numb)
    {
        GCDOracle oracle = new GCDOracle();
        return oracle.gcd(numa, numb);
    }





    @Test(timeout=10)
    public void test1() {

    int actual = gcd(0,0);
    int expected = 0;

	assertEquals(expected, actual);

    }

    @Test(timeout=10)
    public void test2() {

    int actual = gcd(1,1);
    int expected = 1;

    assertEquals(expected, actual);

    }

    @Test(timeout=10)
    public void test3() {

    int actual = gcd(Integer.MAX_VALUE,Integer.MAX_VALUE);
    int expected = Integer.MAX_VALUE;

    assertEquals(expected, actual);

    }

    @Test(timeout=10)
    public void test4() {

    int actual = gcd(0,Integer.MAX_VALUE);
    int expected = Integer.MAX_VALUE;

    assertEquals(expected, actual);

    }

    @Test(timeout=10)
    public void test5() {

    int actual = gcd(0,1);
    int expected = 1;

    assertEquals(expected, actual);

    }

    @Test(timeout=10)
    public void test6() {

    int actual = gcd(1,0);
    int expected = 1;

    assertEquals(expected, actual);

    }

    @Test(timeout=10)
    public void test7() {

    int actual = gcd(Integer.MAX_VALUE,0);
    int expected = Integer.MAX_VALUE;

    assertEquals(expected, actual);

    }

    @Test(timeout=10)
    public void test8() {

    int actual = gcd(Integer.MAX_VALUE,1);
    int expected = 1;

    assertEquals(expected, actual);

    }


}
