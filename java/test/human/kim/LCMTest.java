package human;

import static org.junit.Assert.assertEquals;
import oracles.GCDOracle;
import oracles.LCMOracle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LCMTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    //The LCM oracle method.
    public int lcm(int m, int n)
    {
        LCMOracle oracle = new LCMOracle();
        return oracle.lcm(m, n);
    }
    //could be usefule since,
    //lcm(m, n) = abs(m * n)/gcd(m, n)
    public int gcd(int numa, int numb)
    {
        GCDOracle oracle = new GCDOracle();
        return oracle.gcd(numa, numb);
    }


    /**
     * This test should succeed.
     */
    @Test(timeout=10)
    public void test1() {

    int actual = lcm(0,0);
    int expected = 0;
    assertEquals(expected, actual);
    
    }

    @Test(timeout=10)
    public void test2() {

    int actual = lcm(1,1);
    int expected = 1;
    assertEquals(expected, actual);
    
    }

    @Test(timeout=10)
    public void test3() {

    int actual = lcm(Integer.MAX_VALUE,Integer.MAX_VALUE);
    int expected = Integer.MAX_VALUE;
    assertEquals(expected, actual);
    
    }

    @Test(timeout=10)
    public void test4() {

    int actual = lcm(Integer.MIN_VALUE,Integer.MIN_VALUE);
    int expected = Integer.MIN_VALUE;
    assertEquals(expected, actual);
    
    }

    @Test(timeout=10)
    public void test5() {

    int actual = lcm(Integer.MAX_VALUE,1);
    int expected = Integer.MAX_VALUE;
    assertEquals(expected, actual);
    
    }

    @Test(timeout=10)
    public void test6() {

    int actual = lcm(Integer.MIN_VALUE,1);
    int expected = Integer.MIN_VALUE;
    assertEquals(expected, actual);
    
    }

    @Test(timeout=10)
    public void test7() {

    int actual = lcm(1,Integer.MIN_VALUE);
    int expected = Integer.MIN_VALUE;
    assertEquals(expected, actual);
    
    }

    @Test(timeout=10)
    public void test8() {

    int actual = lcm(1,Integer.MAX_VALUE);
    int expected = Integer.MAX_VALUE;
    assertEquals(expected, actual);
    
    }

}
