package human;

import static org.junit.Assert.assertEquals;
import oracles.StringOracle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StringTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    public int indexOf(String str, String sub) {
	StringOracle strOracle = StringOracle.fromString(str);
	StringOracle subOracle = StringOracle.fromString(sub);
	return strOracle.indexOf(subOracle);
    }

    /**
     * This test should succeed.
     */
    @Test(timeout=10)
    public void test1() {
        int actual = indexOf("spongebob", "bob");
        int expected = 6;
        assertEquals(expected, actual);
    }

    @Test(timeout=10)
    public void test2() {
        int actual = indexOf("", "");
        int expected = 0;
        assertEquals(expected, actual);
    }

    @Test(timeout=10)
    public void test3() {
        int actual = indexOf("a", "");
        int expected = 0;
        assertEquals(expected, actual);
    }

    @Test(timeout=10)
    public void test4() {
        int actual = indexOf("", "a");
        int expected = -1;
        assertEquals(expected, actual);
    }

}
