package human;

import org.junit.*;
import static org.junit.Assert.*;

import oracles.GCDOracle;

public class GCDTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    //The only oracle method.
    public int gcd(int numa, int numb)
    {
        GCDOracle oracle = new GCDOracle();
        return oracle.gcd(numa, numb);
    }





    @Test(timeout=10)
    public void test1() {

    int actual = gcd(3,9);
    int expected = 3;

	assertEquals(expected, actual);

    }
    @Test(timeout=10)
    public void test2() {

    int actual = gcd(9,3);
    int expected = 3;

    assertEquals(expected, actual);

    }

    @Test(timeout=10)
    public void test3() {

    int actual = gcd(0,1);
    int expected = 1;

    assertEquals(expected, actual);

    }


}
