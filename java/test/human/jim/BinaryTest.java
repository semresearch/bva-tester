package human;

import org.junit.*;
import static org.junit.Assert.*;

import oracles.BinaryOracle;

public class BinaryTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    //The only oracle method.
    public int binarySearch(int[] nums, int check)
    {
        BinaryOracle oracle = new BinaryOracle();
        return oracle.binarySearch(nums, check);
    }


    @Test(timeout=10)
    public void test3() {

    int[] haystack = {};
    int needle = 1;

    int actual = binarySearch(haystack,needle);
    int expected = -1;

    assertEquals(expected, actual);

    }

    @Test(timeout=10)
    public void test4() {

    int[] haystack = {0,1,2,3,4};
    int needle = 3;

    int actual = binarySearch(haystack,needle);
    int expected = 3;

    assertEquals(expected, actual);

    }

    @Test(timeout=10)
    public void test5() {

    int[] haystack = {0,1,2,3,4};
    int needle = 20;

    int actual = binarySearch(haystack,needle);
    int expected = -1;

    assertEquals(expected, actual);

    }


}
