package human;

import static org.junit.Assert.*;

import oracles.LCMOracle;
import oracles.GCDOracle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LCMTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    //The LCM oracle method.
    public int lcm(int m, int n)
    {
        LCMOracle oracle = new LCMOracle();
        return oracle.lcm(m, n);
    }
    //could be usefule since,
    //lcm(m, n) = abs(m * n)/gcd(m, n)
    public int gcd(int numa, int numb)
    {
        GCDOracle oracle = new GCDOracle();
        return oracle.gcd(numa, numb);
    }


    /**
     * This test should succeed.
     */
    @Test(timeout=10)
    public void test1() {

    int actual = lcm(0,0);
    int expected = 0;
    assertEquals(expected, actual);
    
    }
    @Test(timeout=10)
    public void test2() {

    int actual = lcm(0,1);
    int expected = 1;
    assertEquals(expected, actual);
    
    }
    @Test(timeout=10)
    public void test3() {

    int actual = lcm(1,1);
    int expected = 1;
    assertEquals(expected, actual);
    
    }
    @Test(timeout=10)
    public void test4() {
    //primes
    int actual = lcm(19,71);
    int expected = 1;
    assertEquals(expected, actual);
    
    }


}
