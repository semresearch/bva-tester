package human;

import static org.junit.Assert.assertEquals;
import oracles.GCDOracle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GCDTest {

	private GCDOracle oracle = new GCDOracle();

	/*
	 * Input boundaries:
	 * 
	 * On: 1, many (in some sense)
	 * 
	 * Off: 0 kind of
	 * 
	 * Lowest result is 1, highest is input
	 * 
	 * Negative behaviour is undefined, don't test.
	 */

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	// @Test(timeout=10)
	// public void testBothZero() {
	// int actual = oracle.gcd(0, 0);
	// int expected = 0;
	//
	// assertEquals(expected, actual);
	// }

	// @Test(timeout=10)
	// public void testLeftNeg() {
	// int actual = oracle.gcd(-1, 0);
	// int expected = 0;
	//
	// assertEquals(expected, actual);
	// }
	//
	// @Test(timeout=10)
	// public void testRightNeg() {
	// int actual = oracle.gcd(0, -1);
	// int expected = 0;
	//
	// assertEquals(expected, actual);
	// }
	//
	// @Test(timeout=10)
	// public void testBothNeg() {
	// int actual = oracle.gcd(-1, -1);
	// int expected = 0;
	//
	// assertEquals(expected, actual);
	// }

	@Test(timeout=10)
	public void testBothOne() {
		int actual = oracle.gcd(1, 1);
		int expected = 1;

		assertEquals(expected, actual);
	}

	@Test(timeout=10)
	public void testRightOne() {
		int actual = oracle.gcd(0, 1);
		int expected = 1;

		assertEquals(expected, actual);
	}

	@Test(timeout=10)
	public void testLeftOne() {
		int actual = oracle.gcd(1, 0);
		int expected = 1;

		assertEquals(expected, actual);
	}

	@Test(timeout=10)
	public void testBothManySame() {
		int actual = oracle.gcd(99, 99);
		int expected = 99;

		assertEquals(expected, actual);
	}

	@Test(timeout=10)
	public void testBothManyDiff() {
		int actual = oracle.gcd(99, 88);
		int expected = 11;

		assertEquals(expected, actual);
	}

	@Test(timeout=10)
	public void testBothManyIndivisible() {
		int actual = oracle.gcd(99, 98);
		int expected = 1;

		assertEquals(expected, actual);
	}

	@Test(timeout=10)
	public void testOneMany() {
		int actual = oracle.gcd(1, 99);
		int expected = 1;

		assertEquals(expected, actual);
	}

	@Test(timeout=10)
	public void testManyOne() {
		int actual = oracle.gcd(99, 1);
		int expected = 1;

		assertEquals(expected, actual);
	}
}
