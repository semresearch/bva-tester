package human;

import static org.junit.Assert.assertEquals;
import oracles.BinaryOracle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BinaryTest {
	private int[] emptyArray;
	private int[] singleArray;
	private int[] manyArray;
	private BinaryOracle oracle = new BinaryOracle();

	@Before
	public void setUp() {
		emptyArray = new int[] {};
		singleArray = new int[] { 42 };
		manyArray = new int[] { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5 };
	}

	@After
	public void tearDown() {

	}

	// TEST CASES

	/*
	 * Array boundary cases:
	 * 
	 * Length: 0, 1, many
	 * 
	 * Check boundaries:
	 * 
	 * Present: true false
	 * 
	 * Position: start end
	 */

	@Test(timeout=10)
	public void testEmpty() {
		assertEquals(-1, oracle.binarySearch(this.emptyArray, 42));
	}

	@Test(timeout=10)
	public void testSinglePresent() {
		assertEquals(0, oracle.binarySearch(this.singleArray, 42));
	}

	@Test(timeout=10)
	public void testSingleNotPresent() {
		assertEquals(-1, oracle.binarySearch(this.singleArray, 1));
	}

	@Test(timeout=10)
	public void testManyPresentStart() {
		assertEquals(0, oracle.binarySearch(this.manyArray, -5));
	}

	@Test(timeout=10)
	public void testManyPresentEnd() {
		assertEquals(this.manyArray.length-1, oracle.binarySearch(this.manyArray, 5));
	}

	@Test(timeout=10)
	public void testManyNotPresent() {
		assertEquals(-1, oracle.binarySearch(this.manyArray, 42));
	}
}
