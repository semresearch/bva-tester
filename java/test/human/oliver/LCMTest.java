package human;

import static org.junit.Assert.assertEquals;
import oracles.LCMOracle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LCMTest {

	private LCMOracle oracle = new LCMOracle();

	@Before
	public void setUp() {

	}

	@After
	public void tearDown() {

	}
	
	/*
	 * Lowest: 0 0
	 * 
	 * 
	 */

	@Test(timeout=10)
	public void testBothZero() {
		int actual = oracle.lcm(0, 0);
		int expected = 0;

		assertEquals(expected, actual);
	}
	@Test(timeout=10)
	public void testOneZero() {
		int actual = oracle.lcm(1, 0);
		int expected = 0;

		assertEquals(expected, actual);
	}
	@Test(timeout=10)
	public void testZeroOne() {
		int actual = oracle.lcm(0, 1);
		int expected = 0;

		assertEquals(expected, actual);
	}

	@Test(timeout=10)
	public void testOneMany() {
		int actual = oracle.lcm(1, 42);
		int expected = 42;

		assertEquals(expected, actual);
	}

	@Test(timeout=10)
	public void testManyOne() {
		int actual = oracle.lcm(42, 1);
		int expected = 42;

		assertEquals(expected, actual);
	}

	@Test(timeout=10)
	public void testManyManySame() {
		int actual = oracle.lcm(42, 42);
		int expected = 42;

		assertEquals(expected, actual);
	}

	@Test(timeout=10)
	public void testManyManyDiff() {
		int actual = oracle.lcm(10, 4);
		int expected = 20;

		assertEquals(expected, actual);
	}

	@Test(timeout=10)
	public void testManyManyVeryDiff() {
		int actual = oracle.lcm(10, 3);
		int expected = 30;

		assertEquals(expected, actual);
	}
}
