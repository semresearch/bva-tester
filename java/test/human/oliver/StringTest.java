package human;

import static org.junit.Assert.assertEquals;
import oracles.StringOracle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StringTest {

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {

	}
	
	private int indexOf(String main, String query) {
		StringOracle oracle = new StringOracle(main.toCharArray());
		return oracle.indexOf(query);
	}
	
	/*
	 * Boundaries:
	 * 
	 * Main: length 0, 1, many
	 * 
	 * Query: length 0, 1, many
	 * 
	 * Query: present: true false
	 * 
	 * Query: present start, end
	 */

	@Test(timeout=10)
	public void testEmptyEmpty() {
		assertEquals(0, indexOf("", ""));
	}
	@Test(timeout=10)
	public void testEmptyOne() {
		assertEquals(-1, indexOf("", "t"));
	}
	@Test(timeout=10)
	public void testEmptyMany() {
		assertEquals(-1, indexOf("", "test"));
	}
	
	@Test(timeout=10)
	public void testOneEmpty() {
		assertEquals(0, indexOf("t", ""));
	}
	@Test(timeout=10)
	public void testManyEmpty() {
		assertEquals(0, indexOf("test", ""));
	}
	
	@Test(timeout=10)
	public void testOneOneTrue() {
		assertEquals(0, indexOf("t", "t"));
	}
	@Test(timeout=10)
	public void testOneOneFalse() {
		assertEquals(-1, indexOf("t", "e"));
	}
	
	@Test(timeout=10)
	public void testManyManyStart() {
		assertEquals(0, indexOf("testing", "test"));
	}
	@Test(timeout=10)
	public void testManyManyEnd() {
		int expected = "testing".length() - "ing".length();
		assertEquals(expected, indexOf("testing", "ing"));
	}
	@Test(timeout=10)
	public void testManyManyFalse() {
		assertEquals(-1, indexOf("testing", "foo"));
	}
	
	@Test(timeout=10)
	public void testManyOneStart() {
		assertEquals(0, indexOf("testing", "t"));
	}
	@Test(timeout=10)
	public void testManyOneEnd() {
		assertEquals("testing".length()-1, indexOf("testing", "g"));
	}
	@Test(timeout=10)
	public void testManyOneFalse() {
		assertEquals(-1, indexOf("testing", "f"));
	}

	@Test(timeout=10)
	public void testOneManyFalse() {
		assertEquals(-1, indexOf("t", "testing"));
	}

}
