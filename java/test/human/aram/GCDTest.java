package human;

import org.junit.*;
import static org.junit.Assert.*;

import oracles.GCDOracle;

public class GCDTest {

    /*
    
    Equivalence Partitioning Blocks and BVA
    (disjoint within blocks, not disjoint between blocks):
    
    I initially did this for all integers, but it was later clarified
    in the interface that it only applies for positive integers...
    
    A: x == y
        On: x=y, Off: x=y-1, y+1
    B: x > y
        On: x=y, Off: x=y+1 
    C: x < y
        On: x=y, Off: x=y-1 
    
    D: x > 0
        On: x=0, Off: x=1
    E: x == 0
        On: x=0, Off: x=-1, x=1
    F: x < 0
        On: x=0, Off: x=1
    
    G: y > 0
        On: y=0, Off: y=1
    H: y == 0
        On: y=0, Off: y=-1, y=1
    I: y < 0
        On: y=0, Off: y=-1
    
    J: x > 1
        On: x=1, Off: x=2
    K: x == 1
        On: x=1, Off: x=0, x=2 
    L: x < 1
        On: x=1, Off: x=0
    
    M: y > 1
        On: y=1, Off: y=2
    N: y == 1
        On: y=1, Off: y=0, y=2
    O: y < 1
        On: y=1, Off: y=0
        
    Summary:
    
    x = y, y-1, y+1, 0, 1, -1, 2
    y = 0, 1, -1, 2
    
    All combinations (excluding conflicts):
    
    (x,y)
    -----
    (0,0) (invalid)
    (1,1)
    (-1,-1) (invalid)
    (2,2)
    
    (-1,0) (invalid)
    (0,1) (invalid)
    (-2,-1) (invalid)
    (1,2)
    
    (1,0) (invalid)
    (2,1)
    (0,-1) (invalid)
    (3,2)
    
    (0,0) (duplicate)
    (0,1) (duplicate)
    (0,-1) (duplicate)
    (0,2) (invalid)
    
    (1,0) (duplicate)
    (1,1) (duplicate)
    (1,-1) (invalid)
    (1,2) (duplicate)
    
    (-1,0) (duplicate)
    (-1,1) (invalid)
    (-1,-1) (duplicate)
    (-1,2) (invalid)
    
    (2,0) (invalid)
    (2,1) (duplicate)
    (2,-1) (invalid)
    (2,2) (duplicate)
     
    */
    
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    //The only oracle method.
    public int gcd(int numa, int numb)
    {
        GCDOracle oracle = new GCDOracle();
        return oracle.gcd(numa, numb);
    }
    
    @Test(timeout=10)
    public void test2() {
        int actual = gcd(1,1);
        int expected = 1;
        assertEquals(expected, actual);
    }
    
    @Test(timeout=10)
    public void test4() {
        int actual = gcd(2,2);
        int expected = 2;
        assertEquals(expected, actual);
    }
    
    // -
    
    @Test(timeout=10)
    public void test8() {
        int actual = gcd(1,2);
        int expected = 1;
        assertEquals(expected, actual);
    }
    
    // -
    
    @Test(timeout=10)
    public void test10() {
        int actual = gcd(2,1);
        int expected = 1;
        assertEquals(expected, actual);
    }
    
    @Test(timeout=10)
    public void test12() {
        int actual = gcd(3,2);
        int expected = 1;
        assertEquals(expected, actual);
    }

}
