package human;

import static org.junit.Assert.assertEquals;
import oracles.LCMOracle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LCMTest {

/*
    
    Equivalence Partitioning Blocks and BVA
    (disjoint within blocks, not disjoint between blocks):
    
    A: x == y
        On: x=y, Off: x=y-1, y+1
    B: x > y
        On: x=y, Off: x=y+1 
    C: x < y
        On: x=y, Off: x=y-1 
    
    D: x > 0
        On: x=0, Off: x=1
    E: x == 0
        On: x=0, Off: x=-1, x=1
    
    F: y > 0
        On: y=0, Off: y=1
    G: y == 0
        On: y=0, Off: y=-1, y=1
    
    H: x > 1
        On: x=1, Off: x=2
    I: x == 1
        On: x=1, Off: x=0, x=2 
    
    J: y > 1
        On: y=1, Off: y=2
    K: y == 1
        On: y=1, Off: y=0, y=2
        
    Summary:
    
    x = y, y-1, y+1, 0, 1, 2
    y = 0, 1, 2
    
    All combinations (excluding conflicts):
    
    (x,y)
    -----
    (0,0)
    (1,1)
    (2,2)
    
    (-1,0) (invalid)
    (0,1)
    (1,2)
    
    (1,0)
    (2,1)
    (3,2)
    
    (0,0) (duplicate)
    (0,1) (duplicate)
    (0,2)
    
    (1,0) (duplicate)
    (1,1) (duplicate)
    (1,2) (duplicate)
    
    (2,0)
    (2,1) (duplicate)
    (2,2) (duplicate)
     
    */
    
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    //The LCM oracle method.
    public int lcm(int m, int n)
    {
        LCMOracle oracle = new LCMOracle();
        return oracle.lcm(m, n);
    }

    @Test(timeout=10)
    public void test1() {
        int actual = lcm(0,0);
        int expected = 0;
        assertEquals(expected, actual);
    }
    
    @Test(timeout=10)
    public void test2() {
        int actual = lcm(1,1);
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test(timeout=10)
    public void test3() {
        int actual = lcm(2,2);
        int expected = 2;
        assertEquals(expected, actual);
    }
    
    // -
    
    @Test(timeout=10)
    public void test4() {
        int actual = lcm(0,1);
        int expected = 0;
        assertEquals(expected, actual);
    }
    
    @Test(timeout=10)
    public void test5() {
        int actual = lcm(1,2);
        int expected = 2;
        assertEquals(expected, actual);
    }
    
    // -
    
    @Test(timeout=10)
    public void test6() {
        int actual = lcm(1,0);
        int expected = 0;
        assertEquals(expected, actual);
    }
    
    @Test(timeout=10)
    public void test7() {
        int actual = lcm(2,1);
        int expected = 2;
        assertEquals(expected, actual);
    }
    
    @Test(timeout=10)
    public void test8() {
        int actual = lcm(3,2);
        int expected = 6;
        assertEquals(expected, actual);
    }
    
    // -
    
    @Test(timeout=10)
    public void test9() {
        int actual = lcm(0,2);
        int expected = 0;
        assertEquals(expected, actual);
    }

    // -
    
    @Test(timeout=10)
    public void test10() {
        int actual = lcm(2,0);
        int expected = 0;
        assertEquals(expected, actual);
    }
    
}
