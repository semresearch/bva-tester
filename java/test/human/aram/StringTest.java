package human;

import static org.junit.Assert.*;
import oracles.StringOracle;

import org.junit.*;

public class StringTest {

    /*
     
    Equivalence Partitioning Blocks
    (disjoint within blocks, not disjoint between blocks):
     
    A: sub exists at start of str
    B: sub exists at end of str
    C: sub exists elsewhere
    D: sub doesn't exist in str
    
    E: sub is longer than str
    F: sub is the same length as str
    G: sub is shorter than str
    
    All combinations:
    
    A & E
    A & F
    A & G
    
    B & E
    B & F (same as A & F)
    B & G
    
    C & E
    C & F (same as A & F)
    C & G
    
    D & E (same as A & E)
    D & F
    D & G
     
    */
    
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    public int indexOf(String str, String sub) {
	StringOracle strOracle = StringOracle.fromString(str);
	StringOracle subOracle = StringOracle.fromString(sub);
	return strOracle.indexOf(subOracle);
    }

    @Test(timeout=10)
    public void test1() {
        assertEquals(-1, indexOf("sponge", "spongebob"));
    }
    
    @Test(timeout=10)
    public void test2() {
        assertEquals(0, indexOf("sponge", "sponge"));
    }
    
    @Test(timeout=10)
    public void test3() {
        assertEquals(0, indexOf("sponge", "spo"));
    }
    
    @Test(timeout=10)
    public void test4() {
        assertEquals(-1, indexOf("sponge", "mrsponge"));
    }
    
    @Test(timeout=10)
    public void test5() {
        assertEquals(3, indexOf("sponge", "nge"));
    }
    
    @Test(timeout=10)
    public void test6() {
        assertEquals(-1, indexOf("sponge", "mrspongebob"));
    }
    
    @Test(timeout=10)
    public void test7() {
        assertEquals(2, indexOf("sponge", "on"));
    }
    
    @Test(timeout=10)
    public void test8() {
        assertEquals(-1, indexOf("sponge", "egnops"));
    }
    
    @Test(timeout=10)
    public void test9() {
        assertEquals(-1, indexOf("sponge", "no"));
    }
    
}
