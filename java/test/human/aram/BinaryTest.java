package human;

import org.junit.*;
import static org.junit.Assert.*;

import oracles.BinaryOracle;

public class BinaryTest {

    /*
    
    Equivalence Partitioning Blocks and BVA
    (disjoint within blocks, not disjoint between blocks):
    
    A: Contains at index = 0
        On: 0, Off: 1
    B: Contains at index = last
        On: last, Off: last - 1
    C: Contains at index > 0 and index < last
        Covered by A and B
    D: Doesn't contain
        
    E: Array size = 0
        On: 0, Off: 1
    F: Array size = 1
        On: 1, Off = 0 and 2
    G: Array size > 1
        On = 1, Off = 2
        
    Summary:
    
    index =    0, 1, last, last - 1
    size =     0, 1, 2
    
    All combinations (excluding conflicts):
    
    (index, size)
    -------------
    (-,0)
    (-,1)
    (-,2)
    
    (0,1)
    (0,2)
    
    (1,2)
     
    */
    
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    //The only oracle method.
    public int binarySearch(int[] nums, int check) {
        BinaryOracle oracle = new BinaryOracle();
        return oracle.binarySearch(nums, check);
    }

    @Test(timeout=10)
    public void test1() {
        int[] haystack = {};
        int needle = 0;
        int expected = -1;
        int actual = binarySearch(haystack, needle);
	assertEquals(expected, actual);
    }
    
    @Test(timeout=10)
    public void test2() {
        int[] haystack = {1};
        int needle = 0;
        int expected = -1;
        int actual = binarySearch(haystack, needle);
        assertEquals(expected, actual);
    }
    
    @Test(timeout=10)
    public void test3() {
        int[] haystack = {1,2};
        int needle = 0;
        int expected = -1;
        int actual = binarySearch(haystack, needle);
        assertEquals(expected, actual);
    }
    
    // --
    
    @Test(timeout=10)
    public void test4() {
        int[] haystack = {0};
        int needle = 0;
        int expected = 0;
        int actual = binarySearch(haystack, needle);
        assertEquals(expected, actual);
    }
    
    @Test(timeout=10)
    public void test5() {
        int[] haystack = {0,1};
        int needle = 0;
        int expected = 0;
        int actual = binarySearch(haystack, needle);
        assertEquals(expected, actual);
    }
    
    // --
    
    @Test(timeout=10)
    public void test6() {
        int[] haystack = {0,1};
        int needle = 1;
        int expected = 1;
        int actual = binarySearch(haystack, needle);
        assertEquals(expected, actual);
    }
    
}
