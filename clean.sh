#!/bin/bash
BASEDIR=$(dirname $0)

cd $BASEDIR

cd ./java/
make clean

cd ./test/
make clean
