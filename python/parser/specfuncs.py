# Module to install specification functions in
# Simply add the functions as methods and refer to them by name in the spec

import copy

class SpecFuncs(object):
    def __init__(self):
        pass
        
    def even(self, value):
        return value % 2 == 0
        
    def odd(self, value):
        return value % 2 == 1
        
    def contains(self, needle, haystack):
        return needle in haystack
        
    def make_contains(self, param_needle, param_haystack):
        """Make some inputs such that comtains holds."""
        new_haystacks = []
        for hon in param_haystack['ons']:
            for non in param_needle['ons']:
                new_hon = copy.deepcopy(hon)
                if param_haystack['py_type'] == list:
                    new_hon.append(non)
                elif param_haystack['py_type'] == str:
                    new_hon += non
                new_haystacks.append(new_hon)
                assert self.contains(non, new_hon)
        param_haystack['ons'] += new_haystacks
        return param_haystack['ons']
        
    def len(self, arr):
        return len(arr)