"""
Prototype Java parser and boundary value analyser.
"""

import re, random, copy, inspect, sys
from specfuncs import SpecFuncs

DEBUG = True

def log(*args):
    if DEBUG:
        msgstr = ' '.join([str(a) for a in args])
        _, filename, line, _, _, _ = inspect.stack()[1]
        print "[INFO] %3d:" % line, msgstr

class SpecParser(object):
    def __init__(self):
        self.funcs = SpecFuncs()
            
    # Smallest units of change for each type
    ATOMS = {
        int: 1,
        float: 0.0001,
        str: 1, # ordinal
        list: [1],
        dict: {'a': 1},
    }
    
    # Template for parameter maps
    PARAM_TEMPLATE = {
        'index': -1,
        'type': None,
        'py_type': None,
        'ons': [],
        'offs': [],
        'subs': [],
    }
    #magical number to define max integer
    maxinteger = 1000000
    
    def _read_file(self, filename):
        """Read in the contents of a file."""
        file = open(filename, 'r')
        text = file.read()
        file.close()
        return text
        
    
    def generate_tests_file(self, filename):
        log("Generating tests for "+filename)
        return self.generate_tests(self._read_file(filename))
        
        
    def generate_tests(self, text):
        """
        Parse a program spec to extract inputs and generate BVA values.
        Takes the content of a file as input.
        """
        # Extract the param details from the signature
        self.params = self._parse_signature(text)
        
        # Build an index-ordered list of params
        index_func = lambda key: self.params[key]['index']
        self.param_list = sorted(self.params.keys(), key=index_func)
        
        comments = self._parse_comments(text)
        
        index = 1
        # Read over each spec comment
        for comment in comments:
            # Save the parameter format to sub later
            self.params[comment['name']]['format'] = comment['format'].strip("'\"")
            
            # Parse the conditions into points
            self._parse_conds(comment)
        
        # Parse the output factors into points
        self._parse_output(text)
            
        # Generate combined points if sub-params exist
        log("Combining sub-params")
            
        for key in self.params:
            param = self.params[key]
            param['ons'].extend(self._combine_subpars(param, 'ons'))
            param['offs'].extend(self._combine_subpars(param, 'offs'))
            
        self._sort_points()
        return self.params
        
        
    def _parse_signature(self, text):
        """Parse the method signature to extract the input parameters."""
        log("> PARSING SIGNATURES")
        # Get the signature string
        # TODO Handle multiple mehtods in a single file
        sig = re.search('public .*\(([^)]+)\)', text)
        
        params = {}
        if sig and sig.group(1):
            inputs = map(self._clean, sig.group(1).split(','))
            log("Parsing signature ", inputs)
            index = 0
            for input in inputs:
                match = re.match('(\w+(?:\[\])?) (\w+)', input)
                params[match.group(2)] = {
                    'index': index,
                    'type': match.group(1),
                    'py_type': self._j2p_type(match.group(1)),
                    'ons': [],
                    'offs': [],
                    'subs': [],
                }
                
                index += 1
        log("Parsed signature into", params)
        log("< SIGNATURES COMPLETE\n")
        return params
        
        
    def _parse_comments(self, text):
        """Parse the Javadoc comments to extract the domain details."""
        # Extract the comments
        log(" > PARSING COMMENTS")
        #                  param name      format   conditions
        comment_regex = r'@param (\w+)(?: ([^[] ]+) )?([^\n]+)?'
        subpar_regex = r'\(([^)]+)\)'
        comments = []
        
        matches = re.findall(comment_regex, text);
        log("Matched comments", matches)
        for comment in matches:
            log("Parsing comment:", comment)
            
            name, format, conds = comment
            log("Got", name, format, conds)
            
            conds = re.sub('[[\]]', '', conds).split(',')
            
            # Update with sub-parameters
            subindex = 1
            for caps in re.findall(subpar_regex, format):
                # Set int as default subparam type
                sub = {
                    'index': subindex,
                    'type': "int",
                    'py_type': int,
                    'ons': [],
                    'offs': [],
                    'subs': [],
                }
                self.params[name]['subs'].append(sub)
                log("Added sub-param", self.params[name]['subs'][-1])
                subindex += 1
            comments.append({'name': name, 'format': format, 'conds': conds})
            log("Comment yields", comments[-1])
        log("< COMMENTS COMPLETE\n")
        return comments
        
        
    def _parse_conds(self, comment):
        log("> PARSING CONDITIONS")
        # The escape character, doubled because it's looking in an escaped string
        self.ESC = '\\\\'
        
        # Define the allowed condition grammar
        SIMPLE_GRAMMAR = [
            '%s(?P<id>\d+(?:\.\d+)?) (?P<op><|>|<=|>=|==|!=) (?P<pt>[^\s]+)' % self.ESC,
            '(?P<pt>[^\s]+) (?P<op><|>|<=|>=|==|!=) %s(?P<id>\d+(?:\.\d+)?)' % self.ESC,
        ]
        
        REGEX_ALL = r'([^%s<>!=]+) (<|>|<=|>=|==|!=) ([^%s<>!=]+)'
        log(comment)
        # Generate points from each condition
        for cond in comment['conds']:
            log("Parsing condition: %s" % cond)
            if cond:
                match = re.search(REGEX_ALL, cond)
                if match:
                    left, operator, right = map(self._clean, match.groups())
                    details = self._strip_details(left, operator, right)
                    # Attempt to match each grammar style
                    gindex = 0
                    for regex in SIMPLE_GRAMMAR:
                        reverse = (gindex == 1)
                        self._regex_for_points(cond, regex, details, reverse)
                        #log("Also cleaned")
                        #self._regex_for_points(details['clean'], regex, details, reverse)
                        gindex += 1
            else:
                param = self.params[comment['name']]
                aux_type = None
                if param['py_type'] == list:
                    aux_type = re.match(r'(\w+)\[\]', param['type']).group(1)
                    aux_type = self._j2p_type(aux_type)
                rand_point = self._random_point(param['py_type'], aux_type)
                param['ons'].append(rand_point)
                log("Generated random", param['py_type'], "value", rand_point)
        log("< CONDITIONS COMPLETE\n")
        
        
    def _parse_output(self, text):
        """Parse conditions affecting the output."""
        log("> PARSING OUTPUT")
        output_regex = r'@return(?: ([^\n]+))?'
        match = re.search(output_regex, text)
        if match and match.group(1):
            log("Matched", match.groups())
            out_conds = re.sub('[[\]]', '', match.group(1)).split(';')
            for cond in out_conds:
                details = self._strip_details(cond)
                if details['left']['funcs']:
                    func, args = details['left']['funcs']
                    result = getattr(self.funcs, 'make_'+func)(*args)
                    log("Result is", result)
        log("> OUTPUT COMPLETE")
        
        
    def _random_point(self, point_type, aux_type=None):
        """Generate a random input when no domain conditions are specified."""
        if point_type == int:
            # return MAX INT
            return self.maxinteger
            #return random.randint(-sys.maxint-1, sys.maxint)
        elif point_type == float:
            return random.random() * self._random_point(int)
        elif point_type == str:
            string = ""
            for i in xrange(30):
                string += chr(random.randint(ord('a'), ord('z')))
            return string
        elif point_type == list:
            return [self._random_point(aux_type)]
        else:
            raise Exception("Cannot create random input for type", point_type)
            
        
    def _regex_for_points(self, condition, regex, details, reverse=False):
        """Apply the given regex to the condition and add any resulting points."""
        log("> REGEXING FOR POINTS")
        log("Using", regex, "on", condition, "reverse?", reverse)
        # Reverse operator of each operator in case the ID is on the right
        OP_REVERSE = {'<': '>', '>': '<', '<=': '>=', '>=': '<='}
        match = re.search(regex, condition)
        if match:
            log("Matched:", match.groups())
            
            # Extract the key values
            id = match.group('id')
            operator = match.group('op')
            point = match.group('pt')
            
            log("ID:", id, ", point:", point)
            
            if '.' in id:
                id, sub_id = id.split('.')
                param = self.params[self.param_list[int(id)-1]]['subs'][int(sub_id)-1]
                log("It's a sub, id =", id+"."+sub_id)
                log("And the param:", param)
            else:
                param = self.params[self.param_list[int(id)-1]]
            log(details)
            param_funcs = details['left']['funcs']
            point_funcs = details['right']['funcs']
            # If the second regex matched, reverse everything
            if reverse:
                operator = OP_REVERSE[operator]
                param_funcs, point_funcs = point_funcs, param_funcs
                
            point = param['py_type'](point)
            
            # Add the on and off points
            log("The py_type is", param['py_type'])
            if param['py_type'] == str:
                new_type = str
                caps = re.findall(r'\(([^)]+)\)', comment['format'])
                reg_type = caps[param['index']]
                # TODO better
                if '\d' in reg_type:
                    new_type = int
                log("The real type of the point is", new_type(point))
                self._add_points(param, operator, new_type(point), param_funcs, point_funcs, new_type=new_type)
            else:
                log("So let's add a point to", param, point)
                self._add_points(param, operator, point, param_funcs, point_funcs)
            assert len(self.PARAM_TEMPLATE['ons']) == 0
        log("< REGEXING COMPLETE")
        
        
    def _strip_details(self, left, operator=None, right=None):
        log("> STRIPPING DETAILS")
        REGEX_ID = r'%s(\d+)' % self.ESC
        
        details = {'op': operator}
        sides = {'left': left}
        if right:
            sides['right'] = right
            
        for side in sides:
            details[side] = {'funcs': {}}
            text = sides[side]
            log(side, "is currently", text)
            
            # Strip off any function calls
            # Each function call is stored as [name, args]
            parent = details[side]['funcs'] = []
            old = None
            while True:
                match = re.match(r'(\w+)\((.+)\)', text)
                if match:
                    log("Matched", match.groups())
                    func, text = match.groups()
                    old = parent
                    parent = []
                    old += [func, parent]
                elif old:
                    ids = re.findall(REGEX_ID, text)
                    for id in ids:
                        id = int(id)
                        parent.append(self.params[self.param_list[id-1]])
                    #details[side]['clean'] = self.ESC+str(id)
                    break
                else:
                    #details[side]['clean'] = text
                    break
            log("Functions on", side, ":", details[side]['funcs'])
        #details['clean'] = '%s %s %s' % (details['left']['clean'], operator, details['right']['clean'])
        #log("Clean result:", details['clean'])
        log("< DETAILS COMPLETE\n")
        return details

    def _add_points(self, param, operator, point, param_funcs, point_funcs, new_type=None):
        """
        Given a parameter and a piece of its specification, add the implied on and
        off points.
        """
        log("> ADDING POINT")
        log("Calculating points for", param, operator, point, "with funcs", param_funcs, point_funcs)
        if new_type:
            log("new type =", new_type)
        temp = {}
        
        if param['py_type'] == list:
            # An empty string should be valid by default
            point_type = 'min'
            temp['ons'] = [[]]
            temp['offs'] = []
        else:
            atom = self.ATOMS[param['py_type']]
            point_type = None
            if operator == '<':
                point_type = 'max'
                temp['offs'] = [point]
                temp['ons']  = [point - atom]
            elif operator == '<=':
                point_type = 'max'
                temp['ons'] = [point]
                temp['offs'] = [point + atom]
            elif operator == '>':
                point_type = 'min'
                temp['offs'] = [point]
                temp['ons'] = [point + atom]
            elif operator == '>=':
                point_type = 'min'
                temp['ons'] = [point]
                temp['offs'] = [point - atom]
            elif operator == '==':
                point_type = 'equal'
                temp['ons'] = [point]
                temp['offs'] = [point - atom, point + atom]
            elif operator == '!=':
                point_type = 'notequal'
                temp['offs'] = [point]
                temp['ons'] = [point - atom, point + atom]
            
        log("Now", temp)
            
        def apply_funcs(point, funcs, point_type):
            # How many neighbours to try for validity
            log("> APPLYING FUNCTION")
            MAX_ATTEMPTS = 1000
            if len(funcs) == 1:
                if point_type == 'notequal':
                    raise Exception("Not equal point type sohuld be handled outside")
                elif point_type == 'equal':
                    return None
                else:
                    log(funcs)
                    
                    attempts = 0
                    while True:
                        values = []
                        for func in funcs:
                            # TODO support additional arguments
                            log("Applying function", funcs[0])
                            values.append(getattr(self.funcs, func)(point))
                            log("Result:", point, "->", values[-1])
                        valid = all(values)
                        
                        # Valid value found
                        if valid and point not in param['ons']:
                            log("Hey,", point, "works!")
                            return point
                        # No valid values found
                        elif attempts > MAX_ATTEMPTS:
                            param['ons'].remove(point)
                            return None
                        # Try a new point
                        else:
                            if point_type == 'max':
                                point -= 1
                            elif point_type == 'min':
                                point += 1
                            attempts += 1
            log("< FUNCTION APPLIED")
        
        log(param_funcs)
        if param_funcs:
            if point_type == 'notequal':
                left = apply_funcs(temp['ons'][0], param_funcs, 'max')
                right = apply_funcs(temp['ons'][1], param_funcs, 'min')
                self._add_points(param, '<=', left, [], [])
                self._add_points(param, '>=', right, [], [])
                return param
            else:
                valid = apply_funcs(temp['ons'][0], param_funcs, point_type)
                if operator[-1] != '=':
                    operator += '='
                return self._add_points(param, operator, valid, [], [])
            
        # TODO remember merge function
        for off in temp['offs']:
            if new_type:    off = param['py_type'](off)
            param['offs'].append(off)
        for on in temp['ons']:
            if new_type:    on = param['py_type'](on)
            param['ons'].append(on)
        log("Added points: on", param['ons'], "and off", param['offs'])
        log("< POINT ADDED")
        return param
                    
    def _combine_subpars(self, param, which):
        """
        Combine the param's sub-params into the param.
        which specifies whether to consider ons or offs.
        """
        final = []
        combos = self._all_combinations(param['subs'], which)
        for combo in combos:
            matches = re.findall(r'\(\\d\+\)', param['format'])
            base = param['format']
            i = 0
            for match in matches:
                base = base.replace(match, str(combo[i]), 1)
                i += 1
            final.append(base)
        return final
            
        # log(params)
        return self.params
        
    def _sort_points(self):
        """Sort the points in asceding order for aesthetics."""
        sort = lambda x: x.sort()
        map(sort, [self.params[key]['ons'] for key in self.params])
        map(sort, [self.params[key]['offs'] for key in self.params])

    def _j2p_type(self, jtype):
        """Map a Java type to an equivalent Python type."""
        type_map = {
            'int': int,
            'Integer': int,
            'float': float,
            'double': float,
            'Float': float,
            'Double': float,
            'String': str,
            
            'int[]': list,
            'float[]': list,
            'double[]': list,
            'String[]': list,
        }
        reg_map = {
            'List<.*>': list,
            'Map<.*, .*>': dict,
        }
        if jtype in type_map:
            return type_map[jtype]
        else:
            for type in reg_map:
                match = re.match(type, jtype)
                if match:
                    return reg_map[type]

    def _clean(self, text):
        """Basic string cleaning function for map."""
        return text.strip()
        
    def _all_combinations(self, params, which, fn=None):
        '''
        Generates all combinations of the given parameters as a list of lists,
        where the inner list is an argument list to be passed to the oracle
        which = ons or offs
        '''
        combs = []
        for param in params:
            points = param[which]
            #points = self.getJavaTypesSyntax(points, param['type'])
            if len(combs) == 0:
                if fn:
                    points = map(fn, points)
                combs = [[x] for x in points]
            else:
                new = []
                for point in points:
                    # `combcopy = [[y for y in x] for x in combs]` may be more efficient
                    # only one level of copying, so strings remain refs, but in practice I saw no difference
                    combcopy = copy.deepcopy(combs)
                    for comb in combcopy:
                        if fn:
                            point = fn(point)
                        comb.append(point)
                    new.extend(combcopy)
                combs = new
        return combs

    def print_result(self, params):
        print "\nRESULTS:"
        for param in params:
            print param
            param = params[param]
            print "  @param on :", param['ons']
            print "  @param off:", param['offs']
            print 
            for sub in param['subs']:
                print "  on :", sub['ons']
                print "  off:", sub['offs']

                
if __name__ == '__main__':
    sc = SpecParser()
    # result = sc.generate_tests_file("tests/SpecSubstring.java")
    result = sc.generate_tests_file("../../java/src/oracles/GCDInterface.java")
    # result = sc.generate_tests_file("../../java/src/oracles/LCMInterface.java")
    # result = sc.generate_tests_file("../../java/src/oracles/BinaryInterface.java")
    # result = sc.generate_tests_file("../../java/src/oracles/StringInterface.java")
    log("> FINAL RESULT")
    sc.print_result(result)