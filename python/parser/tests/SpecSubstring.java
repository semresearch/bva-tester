package sem-research-project;

/**
 * A simple example interface specification to write tests against.
 */
public interface Substringer {
	
	/**
	 * Converts String time values to an integer representation of a date.
	 *
	 * @param query
	 *     The substring to search for
	 * @param target
	 *     The string to search in
	 * @return [contains(\1, \2)]
	 *	   The index of the start of query in string, or -1 if not present
	 */
	public String indexOf(String query, String target);
}