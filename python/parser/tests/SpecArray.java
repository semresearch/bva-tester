package sem-research-project;

/**
 * A simple example interface specification to write tests against.
 */
public interface BinarySearch {
	
	/**
	 * Converts String time values to an integer representation of a date.
	 *
     * @param target
     *     The number to search for
	 * @param list
	 *     The list to search in
	 * @return [contains(\1, \2)]
     *     Index of the given target.
	 */
	public int search(int target, int[] list);
}