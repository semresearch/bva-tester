package sem-research-project;

/**
 * A simple example interface specification to write tests against.
 */
public interface DateParser {
	
	/**
	 * Converts String time values to an integer representation of a date.
	 *
	 * @param time "(\d+):(\d+):(\d+)" [0 <= \1.1 < 24, 0 <= \1.2 < 60, 0 <= \1.3 < 60,]
	 *     The hour of the day
	 * @return 3-tuple of ints: hours, minutes, seconds
	 */
	public String formatTime(String time);
}