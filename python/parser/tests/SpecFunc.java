package sem-research-project;

/**
 * A simple example interface specification to write tests against.
 */
public interface DateFormatter {
	
	/**
	 * Converts integer time values to a string representation of a date.
	 *
	 * @param hours \d+ [0 <= \1 < 24, even(\1) == True]
	 *     The hour of the day
	 * @return String like "HH:MM:SS"
	 */
	public String formatTime(int hours);
}