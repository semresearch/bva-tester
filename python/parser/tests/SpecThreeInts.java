package sem-research-project;

/**
 * A simple example interface specification to write tests against.
 */
public interface DateFormatter {
	
	/**
	 * Converts integer time values to a string representation of a date.
	 *
	 * @param hours \d+ [0 <= \1 < 24]
	 *     The hour of the day
	 * @param minutes \d+ [0 <= \2 < 60]
     *     The minute of the hour 
	 * @param seconds \d+ [0 <= \3 < 60]
	 *     The second of the minute
	 * @return String like "HH:MM:SS"
	 */
	public String formatTime(int hours, int minutes, int seconds);
}