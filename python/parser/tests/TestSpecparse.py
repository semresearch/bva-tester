"""
Test file for specparser
"""

import specparser

global sc
sc = specparser.SpecParser()
    
### TEST TYPE MAPPING

def testMapInt():
    assert sc._j2p_type('int') == int
    assert sc._j2p_type('Integer') == int
    
def testMapFloat():
    assert sc._j2p_type('float') == float
    assert sc._j2p_type('Float') == float
    
def testMapDouble():
    assert sc._j2p_type('double') == float
    assert sc._j2p_type('Double') == float
    
def testMapList():
    assert sc._j2p_type('List<String>') == list
    
def testMapMap():
    assert sc._j2p_type('Map<String, String>') == dict

### TEST FUNCTION INPUT PARSING
    
def testParseNone():
    result = sc._parse_signature('public void testFunc()')
    assert result == {}
    
def testParseInt():
    result = sc._parse_signature('public void testFunc(int hello)')
    assert type(result) == dict
    assert len(result) == 1
    assert 'hello' in result
    assert result['hello']['type'] == 'int'
    assert result['hello']['py_type'] == int
    
def testParseThreeInts():
    result = sc._parse_signature('public int testFunc(int a, int b, int c)')
    assert type(result) == dict
    assert len(result) == 3
    assert set(['a', 'b', 'c']) == set(result.keys())
    assert all([result[key]['type'] == 'int' for key in result])
    assert all([result[key]['py_type'] == int for key in result])
    
### TEST POINT CALCULATION

def testPointsIntLess():
    param = {'type': 'int', 'py_type': int, 'ons': [], 'offs': []}
    sc._add_points(param, '<', 24, [], [])
    assert param['ons'] == [23]
    assert param['offs'] == [24]
    
def testPointsIntLessEven():
    param = {'type': 'int', 'py_type': int, 'ons': [], 'offs': []}
    sc._add_points(param, '<', 24, ['even'], [])
    assert param['ons'] == [22]
    assert param['offs'] == [23]
def testPointsIntGreaterEqualOdd():
    param = {'type': 'int', 'py_type': int, 'ons': [], 'offs': []}
    sc._add_points(param, '>=', 0, ['odd'], [])
    assert param['ons'] == [1]
    assert param['offs'] == [0]
def testPointsIntNotEqualEven():
    param = {'type': 'int', 'py_type': int, 'ons': [], 'offs': []}
    sc._add_points(param, '!=', 0, ['even'], [])
    assert param['ons'] == [-2, 2]
    assert param['offs'] == [-1, 1]
    
# TODO More
    
    
### TEST BVA GENERATION

def testBVAInt():
    result = sc.generate_tests_file("tests/SpecInt.java")
    sc.print_result(result)
    assert type(result) == dict
    assert len(result) == 1
    
def testBVAThreeInts():
    result = sc.generate_tests_file("tests/SpecThreeInts.java")
    sc.print_result(result)
    assert type(result) == dict
    assert len(result) == 3
    assert all([result[key]['type'] == 'int' for key in result])
    assert all([result[key]['py_type'] == int for key in result])
    assert result['hours']['ons'] == [0, 23]
    assert result['hours']['offs'] == [-1, 24]
    
def testBVAMixedIntDouble():
    result = sc.generate_tests_file("tests/SpecMixedIntDouble.java")
    sc.print_result(result)
    assert type(result) == dict
    assert len(result) == 2
    assert result['hours']['type'] == 'int'
    assert result['hours']['py_type'] == int
    assert result['minutes']['type'] == 'double'
    assert result['minutes']['py_type'] == float
    assert result['hours']['ons'] == [0, 23]
    assert result['hours']['offs'] == [-1, 24]
    assert result['minutes']['ons'] == [0.0, 60.0 - sc.ATOMS[float]]
    assert result['minutes']['offs'] == [0 - sc.ATOMS[float], 60.0]

def testBVAStringTime():
    result = sc.generate_tests_file("tests/SpecString.java")
    sc.print_result(result)
    assert type(result) == dict
    assert len(result) == 1
    assert set(result['time']['subs'][0]['ons']) == set([0, 23])
    assert set(result['time']['subs'][1]['ons']) == set([0, 59])
    assert set(result['time']['subs'][2]['ons']) == set([0, 59])
    assert '0:0:0' in result['time']['ons']
    assert '23:59:59' in result['time']['ons']
    assert '24:60:60' in result['time']['offs']
    assert '24:60:60' in result['time']['offs']
    
def testBVABinarySearch():
    result = sc.generate_tests_file("tests/SpecArray.java")
    sc.print_result(result)
    assert type(result) == dict
    assert len(result) == 2
    assert len(result['target']['ons']) == 1
    assert len(result['list']['ons']) == 2
    assert result['target']['ons'][0] in result['list']['ons'][-1]

def testBVAString():
    result = sc.generate_tests_file("tests/SpecSubstring.java")
    sc.print_result(result)
    assert type(result) == dict
    assert len(result) == 2
    assert len(result['query']['ons']) == 1
    assert len(result['target']['ons']) == 2
    assert result['query']['ons'][0] in result['target']['ons'][-1]
    
### REAL INTERFACES

def testGcd():
    result = sc.generate_tests_file("../../java/src/oracles/GCDInterface.java")
    sc.print_result(result)
    assert type(result) == dict
    assert len(result) == 2
    assert all([result[key]['type'] == 'int' for key in result])
    assert all([result[key]['py_type'] == int for key in result])
    assert result['numa']['ons'] == [1]
    assert result['numa']['offs'] == [0]
    assert result['numb']['ons'] == [1]
    assert result['numb']['offs'] == [0]
    
def testLcm():
    result = sc.generate_tests_file("../../java/src/oracles/LCMInterface.java")
    sc.print_result(result)
    assert type(result) == dict
    assert len(result) == 2
    assert all([result[key]['type'] == 'int' for key in result])
    assert all([result[key]['py_type'] == int for key in result])
    assert result['m']['ons'] == [0]
    assert result['m']['offs'] == [-1]
    assert result['n']['ons'] == [0]
    assert result['n']['offs'] == [-1]
    
def testBinarySearch():
    result = sc.generate_tests_file("../../java/src/oracles/BinaryInterface.java")
    sc.print_result(result)
    assert type(result) == dict
    assert len(result) == 2
    
def testIndexOf():
    result = sc.generate_tests_file("../../java/src/oracles/StringInterface.java")
    sc.print_result(result)
    assert type(result) == dict
    assert len(result) == 2