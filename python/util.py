import subprocess

def shell(cmd, returnError=False):
	p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
	c = p.communicate()
	return c if returnError else c[0]

def read_file(filename):
    """Read in the contents of a file."""
    file = open(filename, 'r')
    text = file.read()
    file.close()
    return text

def javaName(className, ext=False):
	return className + ('.java' if ext else '')

def interfaceName(oracleName, ext=False):
	"""Returns oracle interface name for an oracle."""
	return javaName(oracleName + 'Interface', ext)

def oracleName(oracleName, ext=False):
	"""Returns oracle class name for an oracle."""
	return javaName(oracleName + 'Oracle', ext)

def execName(oracleName, ext=False):
	"""Returns oracle exec class name for an oracle."""
	return javaName(oracleName + 'Exec', ext)

def testName(oracleName, ext=False):
	"""Returns oracle test class name for an oracle."""
	return javaName(oracleName + 'Test', ext)
