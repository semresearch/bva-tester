import re
import difflib

#These are for testing the helper functions...
FILEIN = 'Test.java'
FILEOUT1 = 'Test1.java'
FILEOUT2 = 'Test2.java'

ATFILEIN = 'SimpleDateFormatOracle.java'
ATFILEOUT = 'SimpleDateFormatOracle-NoAt.java'


pattern = re.compile('<[A-Za-z0-9, ]+>')
replace_dict = []

def removeAtSymbols(filename_in, filename_out):

    fileString = open(filename_in, 'r').read()

    #replace any line that has an at symbol followed by anything that isn't a space with nothing
    new_string = re.sub('@[^ ]*', '', fileString)

    file = open(filename_out, 'w')
    file.write(new_string)


def replaceGenerics(filename_in, filename_out):

    new = ""
    prev = 0

    fileString = open(filename_in, 'r').read()

    #remove all generics and remember where we removed them from
    iterator = pattern.finditer(fileString)
    for match in iterator:

        start = match.span()[0]
        end = match.span()[1]

        new = new + fileString[prev:start]
        replace_dict.append( (start, end, match.group()) )

        prev = end

    new = new + fileString[prev:]

    file = open(filename_out, 'w')
    file.write(new)
        
    #print new

#put it back together
def putBackGenerics(filename_in, filename_out):

    fileString = open(filename_in, 'r').read()

    prev = 0
    for start,end,item in replace_dict:
        fileString = fileString[:start] + item + fileString[start:]
        prev = end

    file = open(filename_out, 'w')
    file.write(fileString)
    #print fileString

def lineDiff(file1, file2):
    lines1 = open(file1, "r").readlines()
    lines2 = open(file2, "r").readlines()

    for lineno in range(0,len(lines1)):
        if lines1[lineno] != lines2[lineno]:
            #print "error on line: %d" % i
            #print lines1[i]
            #print lines2[i]

            end = 0
            size_diff = len(lines1[lineno]) - len(lines2[lineno])

            #check if they aren't the same length
            if(size_diff > 0):
                end = len(lines2[lineno])
            elif (size_diff < 0):
                end = len(lines1[lineno])

            #loop through lines which are of different length only
            if end > 0:
                for charno in range(0,end):
                    if lines1[lineno][charno] != lines2[lineno][charno]:
                        return (lineno, charno, size_diff)


######run the stuff here
if __name__=="__main__":
    #replaceGenerics(FILEIN, FILEOUT1)
    #putBackGenerics(FILEOUT1, FILEOUT2)
    #print "%s and %s should be the same. %s should have all generics removed!" % (FILEIN, FILEOUT2, FILEOUT1)
    #print lineDiff('Test1.java', 'Test3.java')
    removeAtSymbols(ATFILEIN, ATFILEOUT)


