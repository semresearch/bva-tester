package oracles;

public class GCDOracle implements GCDInterface {

    public GCDOracle() {
    }

    public int gcd(int numa, int numb) {
        if (numa < 0)
            {return numb;}
        if (numb == 0)
            {return numa;}
        if (numa > numb)
            {return gcd(numb, numa % numb);}
        return gcd(numa, numb % numa);
    }

}
