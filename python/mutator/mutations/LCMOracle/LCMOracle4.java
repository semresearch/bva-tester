package oracles;

/**
 * Oracle for Least common multiple
 * 
 *  http://rosettacode.org/wiki/Least_common_multiple#Java
 */
public class LCMOracle implements LCMInterface {

    public int lcm(int m, int n) {
        int lcm = (n == m || n == 1) ? m : (m < 1 ? n : 0);
        if (lcm == 0) {
            int mm = m, nn = n;
            while (mm != nn) {
                while (mm < nn) {
                    mm += m;
                }
                while (nn < mm) {
                    nn += n;
                }
            }
            lcm = mm;
        }
        return lcm;
    }

}
