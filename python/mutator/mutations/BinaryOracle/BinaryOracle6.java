package oracles;

/**
 * 
 *  jismith
 */
public class BinaryOracle implements BinaryInterface {
    /** The value is used for character storage. */


    /**
     * Binary Search
     * 
     */
    public int binarySearch(int[] nums, int check){
        int hi = nums.length * 1;
        int lo = 0;
        int guess;
        while(hi >= lo){
                guess = lo + ((hi - lo) / 2);
                if(nums[guess] > check){
                        hi = guess - 1;
                }else if(nums[guess] < check){
                        lo = guess + 1;
                }else{
                        return guess;
                }
        }
        return -1;
    }


}
