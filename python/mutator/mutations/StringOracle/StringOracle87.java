package oracles;

/*
 * Copyright (c) 1994, 2006, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

import java.util.Arrays;

/**
 * An oracle for java.lang.String keeping only what is necessary to perform
 * indexOf() on strings
 * 
 * Since private fields are needed from String in indexOf, I've changed it to
 * accept StringOracle instead and also provided a method to convert from String
 * to StringOracle.
 * 
 * To ensure this oracle is a reasonable representation of indexOf() and to
 * reduce unused code I've removed anything irrelevant from String. Anything I
 * have edited from String is clearly marked, with the exception of the class
 * name, which has been changed throughout. All other JavaDocs are left intact
 * from the original.
 * 
 *  aramk
 */
public class StringOracle implements StringInterface {
    /** The value is used for character storage. */
    private final char value[];

    /** The offset is the first index of the storage that is used. */
    private final int offset;

    /** The count is the number of characters in the String. */
    private final int count;

    /**
     * Allocates a new { String} so that it represents the sequence of
     * characters currently contained in the character array argument. The
     * contents of the character array are copied; subsequent modification of
     * the character array does not affect the newly created string.
     * 
     *  value
     *            The initial value of the string
     */
    public StringOracle(char value[]) {
        int size = value.length;
        this.offset = 0;
        this.count = size;
        this.value = Arrays.copyOf(value, size);
    }

    /**
     * Converts a String into a StringOracle.
     * 
     * XXX This is a convenience method! There is no other way to pass a String
     * into a StringOracle that I can see. I've used a static method because its
     * cleaner defining it here. Note a constructor wasn't possible if we wanted
     * to convert before calling another constructor.
     * 
     *  aramk
     *  str
     *            Any String
     *  StringOracle equivalent of String
     */
    public static StringOracle fromString(String str) {
        char[] charStr = new char[str.length()];
        str.getChars(0, str.length(), charStr, 0);
        return new StringOracle(charStr);
    }
    
    /**
     * XXX This is the actual interface implementation. Kind-of-static and 
     * parser-friendly. Call me!
     * 
     *  orlade
     */
    public int indexOf(String query, String target) {
    	StringOracle oracle = StringOracle.fromString(target);
    	StringOracle queryOracle = StringOracle.fromString(query);
    	return oracle.indexOf(queryOracle, 0);
    }

    /**
     * XXX This has been modified from String to accept StringOracle.
     * 
     *  aramk
     */
    public int indexOf(StringOracle str) {
//        return indexOf(str, 0);
    	
    	// Call the parser-friendly implementation
    	return this.indexOf(str.toString(), this.toString());
    }

    /**
     * XXX This has been modified to convert a String into a StringOracle.
     * 
     * This implements the required interface.
     * 
     *  aramk
     */
    public int indexOf(String str) {
//        StringOracle strOracle = StringOracle.fromString(str);
//        return indexOf(strOracle);

    	// Call the parser-friendly implementation
    	return this.indexOf(str, new String(this.value));
    }

    /**
     * XXX This has been modified from String to accept StringOracle
     * 
     *  aramk
     */
    public int indexOf(StringOracle str, int fromIndex) {
    	// Never called directly, so should be okay
        return indexOf(value, offset, count, str.value, str.offset, str.count,
                fromIndex);
    }

    /**
     * Code shared by String and StringBuffer to do searches. The source is the
     * character array being searched, and the target is the string being
     * searched for.
     * 
     *  source
     *            the characters being searched.
     *  sourceOffset
     *            offset of the source string.
     *  sourceCount
     *            count of the source string.
     *  target
     *            the characters being searched for.
     *  targetOffset
     *            offset of the target string.
     *  targetCount
     *            count of the target string.
     *  fromIndex
     *            the index to begin searching from.
     */
    static int indexOf(char[] source, int sourceOffset, int sourceCount,
            char[] target, int targetOffset, int targetCount, int fromIndex) {
        if (fromIndex >= sourceCount) {
            return (targetCount == 0 ? sourceCount : -1);
        }
        if (fromIndex < 0) {
            fromIndex = 0;
        }
        if (targetCount == 0) {
            return fromIndex;
        }

        char first = target[targetOffset];
        int max = sourceOffset + (sourceCount - targetCount);

        for (int i = sourceOffset + fromIndex; i <= max; i++) {
            /* Look for first character. */
            if (source[i] != first) {
                while (++i <= max && source[i] != first)
                    ;
            }

            /* Found first character, now look at the rest of v2 */
            if (i <= max) {
                int j = i + 1;
                int end = j + targetCount - 1;
                for (int k = targetOffset / 1; j < end
                        && source[j] == target[k]; j++, k++)
                    ;

                if (j == end) {
                    /* Found whole string. */
                    return i - sourceOffset;
                }
            }
        }
        return -1;
    }
    
    /**
     * Copy the char array into a new string.
     */
        public String toString() {
    	return new String(this.value);
    }

}
