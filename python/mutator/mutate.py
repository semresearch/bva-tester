import os, filecmp, sys

#some helper functions related to regular expressions and removing unwanted text from the oracles.
from reg import *

#Written by Kim Braztel
#bratzelk@student.unimelb.edu.au

#usage: python mutate.py <javafile (without .java)>

#This program runs a program which generates a random mutant
#It runs it MAX_MUTATIONS number of times
#It then removes any mutants which aren't unique

#Max number of mutations
MAX_MUTATIONS = 500
OUTPUT_DIR = "mutations"

#directory where the original source code is
SOURCE_DIR = "../../java/src/oracles"

#we will save the oracle to this file while we work on it...
TMP_FILENAME = "tmp_oracle.java"

#get the filename from command line args
try:
    #The filename to mutate, without .java
    FILE = sys.argv[1]
except IndexError:
    print "usage: python mutate.py <javafile (without .java)>"
    exit(1)

#quickly check if the file exists!
try:
   with open('%s/%s.java' % (SOURCE_DIR,FILE)) as f: pass
except IOError as e:
   print "File: %s/%s.java does not exist!" % (SOURCE_DIR, FILE)
   exit(1)


#basically testing system calls...
p = os.system('echo "My breathtaking mutant generator" ')

#create the output directory (if it doesn't exist)
p = os.system('mkdir %s' % OUTPUT_DIR)
#and the folder for the set of mutations
p = os.system('mkdir %s/%s' % (OUTPUT_DIR,FILE))

#remove lines starting with @ (as they screw with the mutator)
#save the modified file to TMP_FILENAME
removeAtSymbols("%s/%s.java" % (SOURCE_DIR, FILE), TMP_FILENAME)

#mutate the program
#lava.jar makes 1 random mutation - which isn't very useful
#so we will make heaps and remove non-unique mutations

for count in range(MAX_MUTATIONS):
    #print "Mutation %d" % count

    mutant_filename = "%s/%s/%s%d.java" % (OUTPUT_DIR, FILE, FILE, count)
    #create the mutant! (using the tmp file we created)
    #p = os.system('java -jar ../../java/lib/lava.jar %s/%s.java > %s' % (SOURCE_DIR, FILE, mutant_filename))
    p = os.system('java -jar ../../java/lib/lava.jar %s > %s' % (TMP_FILENAME, mutant_filename))

    #print whether the files are equal or not (to check a mutant has changed)
    #print filecmp.cmp(mutant_filename, '%s.java' % FILE)


#keep a list of mutants we are deleting
deleted_mutants = []

#Now that they have all been made, let's remove the redundant ones
for mutant_number in range(MAX_MUTATIONS):
    #compare against all remaining mutants
    for remaining_mutants in range(mutant_number, MAX_MUTATIONS):

        #skip this if we are comparing the same files!
        if (mutant_number == remaining_mutants):
            continue;
        #also skip if we've already deleted this mutant
        elif (mutant_number in deleted_mutants or remaining_mutants in deleted_mutants ):
            continue;

        mutant_a = "%s/%s/%s%d.java" % (OUTPUT_DIR, FILE, FILE, mutant_number)
        mutant_b = "%s/%s/%s%d.java" % (OUTPUT_DIR, FILE, FILE, remaining_mutants)

        #if they're the same
        if( filecmp.cmp(mutant_a,mutant_b)):
            #print "%d %d are the same!" % (mutant_number, remaining_mutants)
            #print "Deleting %d" % remaining_mutants
            deleted_mutants.append(remaining_mutants)
            os.remove(mutant_b)


#finished!
unique_mutants = MAX_MUTATIONS - len(deleted_mutants)
print "Number of unique mutants created: %d" % unique_mutants
