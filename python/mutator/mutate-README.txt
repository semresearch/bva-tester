usage: python mutate.py <javafile (without .java)>

The program uses a dodgy library to generate unique mutants.
They are, by default made in the mutant directory in the folder when the program is run.

The library used can be found here:
http://www.doc.gold.ac.uk/~mas01sd/mutants/index.html
There is some information on how it works etc etc.

The library it uses is really weird and won't work on .java files which contain an "@" symbol.
I have had to remove these from any source files that I used to generate mutants for.
We could probably add something to this program to automatically remove these and then put them back,
but I don't think it's really worth it...

Kim.