import static org.junit.Assert.assertEquals;
import oracles.StringOracle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StringTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    public int indexOf(String str, String sub) {
	StringOracle strOracle = StringOracle.fromString(str);
	StringOracle subOracle = StringOracle.fromString(sub);
	return strOracle.indexOf(subOracle);
    }

    /**
     * This test should succeed.
     */
    @Test
    public void test1() {
	assertEquals(6, indexOf("spongebob", "bob"));
    }

    /**
     * This test should fail.
     */
    @Test
    public void test2() {
	assertEquals(6, indexOf("spongebob", "pat"));
    }

}
