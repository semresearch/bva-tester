package auto;

import oracles.<cmp:oracleName/>Oracle;

import org.junit.*;
import static org.junit.Assert.*;

<cmp:imports/>

public class <cmp:oracleName/>Test {

    <cmp:attributes/>

    @Before
    public void setUp() throws Exception {
    	<cmp:setUp/>
    }

    @After
    public void tearDown() throws Exception {
    	<cmp:tearDown/>
    }

    <cmp:methods/>

    <cmp:test>
    @Test(timeout=50)
    public void <cmp:testName/>() {
		<cmp:testContent/>
    }
    </cmp:test>

}
