import re, copy
from .. import util

class JUnitCompiler:
    '''
    Compiles JUnit drivers with given parameters and a Java class
    @author aramk
    '''
    
    compilerDir = '.'
    javaDir = '.'
    oracleDir = '.'
    # A JUnit template driver with <tags> for replacing with values
    tplPath = 'template.java'
    # Keeps a cache of the template
    driverTpl = None
    testTpl = None
    # Keeps a map of strings for the Exec file of each oracle
    execs = {}

    # Tag names
    nameTag = 'oracleName'
    importTag = 'imports'
    setUpTag = 'setUp'
    tearDownTag = 'tearDown'
    methodsTag = 'methods'
    attributesTag = 'attributes'
    testTag = 'test'
    testNameTag = 'testName'
    testContentTag = 'testContent'

    # Tag regex
    closedTag = r"<\s*cmp\s*:%s\s*/\s*>"
    openTag = r"<\s*cmp\s*:%s\s*>(.*?)<\s*/\s*cmp\s*:\s*%s\s*>"
    inTag = r"[^>]+"
    # Not supported in Python < 2.7
    flags = re.M | re.S | re.I
    flagstr = r"(?msi)"
    syms = '[^;{}]+'

    def compile(self, oracleName, methodName, params):
        if self.driverTpl is None:
            self.driverTpl = util.read_file(self.compilerDir + self.tplPath)
            self.testTpl = self.getTagContents(self.testTag, self.driverTpl)
            if len(self.testTpl) == 0:
                raise Exception("No test method given in template")
            self.execs[oracleName] = util.read_file(self.oracleDir + '/' + util.execName(oracleName, True))

        driver = self.driverTpl
        driver = self.replaceTag(self.nameTag, oracleName, driver)
        execs = self.execs[oracleName]
        imports = self.getImports(execs)
        driver = self.replaceTag(self.importTag, "\n".join(imports), driver)
        instance = self.getInstance(oracleName, execs)
        if instance:
            driver = self.replaceTag(self.attributesTag, instance, driver)
        # else:
        #   raise Exception("No instance of oracle found in execution class")
        execMethod, outputType = self.getMethod(methodName, execs)
        methods = []
        methods.append(execMethod + "\n")

        # All combinations of arguments and expected outputs as strings
        combs = self.allCombinations(params) #, lambda x: str(x))

        # Generate expected outputs using the non-mutated oracle
        # This passes all argument combinations at once and exec class returns all expected outputs,
        # which substantially reduces the overhead of starting the JVM for each combination
        flat = [str(x) for y in combs for x in y]
        outputStr = self.execOracle(oracleName, methodName, flat)
        outputs = outputStr.split()

        if len(outputs) == 0:
            raise Exception("Compiler received no outputs when calling oracle execution class")

        # Compile the JUnit driver and add test methods with arguments and expected outputs
        for i in xrange(len(combs)):
            args = combs[i]
            # Generate expected outputs using the non-mutated oracle, slower (see above)
            # output = self.execOracle(oracleName, methodName, args)
            output = outputs[i]
            # Construct a test case using the first template given
            test = self.testTpl[0]
            test = self.replaceTag(self.testNameTag, '%s%d' % (self.testTag, i+1), test)
            output = self.getJavaTypeSyntax(output, outputType)
            method = self.createMethod(methodName, args, semi=False)
            assertion = self.createAssert(output, method)
            test = self.replaceTag(self.testContentTag, assertion, test)
            methods.append(test)

        driver = self.replaceTag(self.methodsTag, "".join(methods), driver)

        # Remove all other tags for valid Java output
        driver = self.clearTags(driver)
        return driver

    def getMethod(self, methodName, javaFile):
        m = self.__search(r"(?!\s)%sstatic%s?(\w+)\s+%s\(.*?return\s*[^;]+;\s*\}" % (self.syms, self.syms, methodName), javaFile, True)
        return m.group(0), m.group(1)

    def getJavaTypeSyntax(self, var, javaType):
        '''
        Converts a var string and a given Java type into valid syntax.
        Note: ints are kept as their original var strings.
        '''
        # TODO doesn't support arrays yet
        if javaType == 'String':
            return '"%s"' % var
        else:
            return var

    def getJavaTypesSyntax(self, lst, javaType):
        return map(lambda x: self.getJavaTypeSyntax(x, javaType), lst)

    def getOutputType(self, methodName, javaFile):
        return self.__search(r"(\w+)\s+%s\s*\(" % methodName, javaFile, True).group(1)

    def getInstance(self, oracleName, javaFile):
        return self.__search(r"(?!\s)%sstatic%snew\s+%s[^;]+;" % (self.syms, self.syms, util.oracleName(oracleName)), javaFile)

    def getImports(self, javaFile):
        return self.__findall(r"import[^;]+;", javaFile)

    def createAssert(self, lhs, rhs):
        return self.createMethod('assertEquals', [lhs, rhs])

    def createMethod(self, name, args, semi=True):
        # argStr = []
        for i in range(len(args)):
            # if (type(args[i]) == str):
                # args[i] = '"'+args[i]+'"'
            if (type(args[i]) == int):
                args[i] = str(args[i])
            elif (type(args[i]) == list):
                '''Only supports int primitives for now'''
                args[i] = 'new int[]{%s}' % ",".join([str(x) for x in args[i]])
        return name + '(' + ", ".join(args) + ')' + (';' if semi else '')

    def execOracle(self, oracleName, methodName, args):
        argsStr = " ".join('"'+x+'"' for x in args)
        cmd = 'java -cp %s oracles.%s %s %s' % (self.javaDir, util.execName(oracleName), methodName, argsStr)
        return util.shell(cmd)

    def allCombinations(self, params, fn=None):
        '''
        Generates all combinations of the given parameters as a list of lists,
        where the inner list is an argument list to be passed to the oracle
        '''
        combs = []
        sortedParams = sorted(params.values(), key=lambda k: k['index']) 
        for param in sortedParams:
            points = param['ons'] + param['offs']
            points = self.getJavaTypesSyntax(points, param['type'])
            if len(combs) == 0:
                if fn:
                    points = map(fn, points)
                combs = [[x] for x in points]
            else:
                new = []
                for point in points:
                    # `combcopy = [[y for y in x] for x in combs]` may be more efficient
                    # only one level of copying, so strings remain refs, but in practice I saw no difference
                    combcopy = copy.deepcopy(combs)
                    for comb in combcopy:
                        if fn:
                            point = fn(point)
                        comb.append(point)
                    new.extend(combcopy)
                combs = new
        return combs

    def getTagContents(self, name, target):
        return self.__findall(self.openTag % (name, name), target)

    def replaceTag(self, name, repl, target):
        # Closed tags
        target = self.__sub(self.closedTag % name, repl, target)
        # Open tags
        target = self.__sub(self.openTag % (name, name), repl, target)
        return target

    def clearTags(self, target):
        target = self.__sub(self.closedTag % self.inTag, '', target)
        target = self.__sub(self.openTag % (self.inTag, self.inTag), '', target)
        return target

    def __sub(self, pattern, repl, target, removeBacks=True):
        '''
        If removeBacks == True, backreferences are removed from repl.
        '''
        if removeBacks:
            repl = repl.replace("\\", "\\\\")
        return re.sub(self.flagstr + pattern, repl, target)

    def __findall(self, pattern, target):
        return re.findall(self.flagstr + pattern, target, flags=self.flags)

    def __search(self, pattern, target, groups=False):
        m = re.search(self.flagstr + pattern, target, flags=self.flags)
        if groups:
            return m
        else:
            return m.group(0) if m else None
